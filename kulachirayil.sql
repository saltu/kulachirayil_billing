-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 25, 2019 at 06:30 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kulachirayil`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `name`, `amount`, `created_at`, `updated_at`) VALUES
(1, 'Account', -554171, NULL, NULL),
(2, 'Sales', 812000, NULL, NULL),
(3, 'Purchase', 1366794, NULL, NULL),
(4, 'Expense', 121, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `phone`, `amount`, `created_at`, `updated_at`) VALUES
(1, 'dxc', '1234569870', NULL, '2019-03-08 00:36:53', '2019-03-08 00:36:53'),
(2, 'NIthn', '9847296004', NULL, '2019-03-08 00:38:33', '2019-03-08 00:38:33'),
(6, 'sd', '1234569870', NULL, '2019-03-11 23:21:11', '2019-03-11 23:21:11'),
(7, 'AKHIL V', '9846982342', 0, '2019-04-22 07:08:09', '2019-04-22 07:08:09'),
(8, 'AKHIL V', '9846982342', 0, '2019-04-24 01:34:02', '2019-04-24 01:34:02');

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE `expenses` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `ex_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `expenses`
--

INSERT INTO `expenses` (`id`, `description`, `amount`, `ex_date`, `created_at`, `updated_at`) VALUES
(1, 'Tea', 100, '2019-03-06', '2019-03-06 06:31:57', '2019-03-06 06:31:57'),
(2, 'zx', 12, '2019-03-12', '2019-03-12 11:34:25', '2019-03-12 11:34:25'),
(3, 'scx', 121, '2019-03-12', '2019-03-12 11:35:30', '2019-03-12 11:35:30');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_nums`
--

CREATE TABLE `invoice_nums` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoice_num` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoice_nums`
--

INSERT INTO `invoice_nums` (`id`, `name`, `invoice_num`, `created_at`, `updated_at`) VALUES
(1, 'Sales', 7, NULL, NULL),
(2, 'Purchase', 21, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_03_04_053630_create_products_table', 2),
(4, '2019_03_04_070900_create_expenses_table', 3),
(5, '2019_03_04_102935_create_invoice_nums_table', 4),
(6, '2019_03_04_070830_create_sales_bills_table', 5),
(7, '2019_03_08_053700_create_customers_table', 5),
(8, '2019_03_08_053806_create_sales_bill_temps_table', 5),
(9, '2019_03_09_053954_create_sales_payments_table', 6),
(10, '2019_03_04_070954_create_accounts_table', 7),
(11, '2019_03_11_040031_create_purchase_payments_table', 8),
(12, '2019_03_11_040048_create_purchase_bills_table', 8),
(13, '2019_03_11_040057_create_purchase_bill_temps_table', 8),
(14, '2019_03_11_040118_create_purchasers_table', 8),
(15, '2019_03_04_071041_create_open_closes_table', 9);

-- --------------------------------------------------------

--
-- Table structure for table `open_closes`
--

CREATE TABLE `open_closes` (
  `id` int(10) UNSIGNED NOT NULL,
  `current_date` date DEFAULT NULL,
  `opening` double DEFAULT NULL,
  `closing` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `open_closes`
--

INSERT INTO `open_closes` (`id`, `current_date`, `opening`, `closing`, `created_at`, `updated_at`) VALUES
(1, '2019-03-12', 56914, NULL, '2019-03-25 01:38:51', '2019-03-25 01:38:51'),
(2, '2019-03-28', 56914, NULL, '2019-03-27 23:34:54', '2019-03-27 23:34:54'),
(3, '2019-03-30', 56914, NULL, '2019-03-30 00:54:31', '2019-03-30 00:54:31'),
(4, '2019-04-02', 56914, NULL, '2019-04-01 23:54:13', '2019-04-01 23:54:13'),
(5, '2019-04-03', 56914, NULL, '2019-04-02 23:19:27', '2019-04-02 23:19:27'),
(6, '2019-04-04', 56914, NULL, '2019-04-03 23:16:44', '2019-04-03 23:16:44'),
(7, '2019-04-05', 56914, NULL, '2019-04-04 23:27:38', '2019-04-04 23:27:38'),
(8, '2019-04-06', 56914, NULL, '2019-04-05 23:44:27', '2019-04-05 23:44:27'),
(9, '2019-04-08', 56914, NULL, '2019-04-07 23:31:16', '2019-04-07 23:31:16'),
(10, '2019-04-09', 56914, NULL, '2019-04-08 23:00:26', '2019-04-08 23:00:26'),
(11, '2019-04-10', 56914, NULL, '2019-04-10 01:27:17', '2019-04-10 01:27:17'),
(12, '2019-04-11', 57014, NULL, '2019-04-10 23:46:09', '2019-04-10 23:46:09'),
(13, '2019-04-12', -412986, NULL, '2019-04-12 01:08:25', '2019-04-12 01:08:25'),
(14, '2019-04-16', -412986, NULL, '2019-04-16 03:02:26', '2019-04-16 03:02:26'),
(15, '2019-04-17', -412986, NULL, '2019-04-17 05:39:00', '2019-04-17 05:39:00'),
(16, '2019-04-19', -412986, NULL, '2019-04-19 02:07:32', '2019-04-19 02:07:32'),
(17, '2019-04-22', -412986, NULL, '2019-04-22 01:14:20', '2019-04-22 01:14:20'),
(18, '2019-04-24', 197914, NULL, '2019-04-24 00:55:56', '2019-04-24 00:55:56'),
(19, '2019-04-25', -106171, NULL, '2019-04-24 23:32:41', '2019-04-24 23:32:41');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purchased` int(11) DEFAULT NULL,
  `gst` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `unit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `code`, `name`, `purchased`, `gst`, `quantity`, `rate`, `unit`, `created_at`, `updated_at`) VALUES
(1, '123', 'iphone', 2, 12, 28, 2777, 'KG', '2019-03-09 01:37:05', '2019-04-25 00:54:41'),
(2, '321', 'oppo', 2, 6, -2, 10000, 'Litre', '2019-03-09 01:37:29', '2019-03-09 01:37:29'),
(3, 'sd', 'sd', 2, 2, 1, 1, 'sd', '2019-03-11 03:10:44', '2019-03-11 03:10:44'),
(4, 'aaa', 'aa', 2, 12, 1, 1000, 'aa', '2019-03-11 04:47:57', '2019-03-11 04:47:57'),
(5, 'sdf', 'sdf', 2, 12, 1, 1111, 'sdf', '2019-03-12 11:39:58', '2019-03-12 11:39:58'),
(6, 'sdfx', 'sdf', 2, 12, 1, 12, 'xvc', '2019-03-12 11:39:58', '2019-03-12 11:39:58'),
(7, 'sdf', 'sdf', 2, 2, 1, 1, 'sdf', '2019-03-12 11:39:58', '2019-03-12 11:39:58'),
(8, '1ewds1', '12e', 2, 2, 9, 1, 'ds', '2019-03-12 11:39:58', '2019-03-12 11:39:58'),
(9, '1', 'dcx', 2, 2, 1, 1, 'cx', '2019-03-12 11:39:58', '2019-03-12 11:39:58'),
(70, '122', 'ppo', 2, 6, 11, 1000, 'Litre', '2019-04-24 07:12:52', '2019-04-24 07:12:52'),
(11, 'cxv', 'sf', 2, 12, 1, 1212, '0', '2019-03-15 06:15:15', '2019-03-15 06:15:15'),
(12, 'asd', 'asd', 2, 10, 3, 123, 'asd', '2019-03-15 06:18:18', '2019-03-15 06:18:18'),
(80, '123', 'iphone10', 2, 12, 10, 4777, 'KG', '2019-04-25 00:54:41', '2019-04-25 00:54:41'),
(79, '123', 'iphone12', 2, 12, 10, 4777, 'KG', '2019-04-25 00:51:58', '2019-04-25 00:51:58'),
(16, 'sd', 'zscv', 4, 10, 1, 12, 'zsd', '2019-03-15 06:28:38', '2019-03-15 06:28:38'),
(71, '123', 'iphone7', 2, 12, 20, 3777, 'KG', '2019-04-25 00:32:33', '2019-04-25 00:32:33'),
(72, '22', 'iphone7', 2, 6, 2, 10000, 'Gram', '2019-04-25 00:32:33', '2019-04-25 00:32:33'),
(73, '123', 'iphone5', 2, 12, 10, 4777, 'KG', '2019-04-25 00:39:57', '2019-04-25 00:39:57'),
(74, '111', 'qqq2', 2, 6, 5, 1020, 'Pieces', '2019-04-25 00:39:57', '2019-04-25 00:39:57'),
(75, '123', 'iphone11', 2, 12, 10, 4777, 'KG', '2019-04-25 00:46:14', '2019-04-25 00:46:14'),
(76, '111', 'qqq', 2, 6, 20, 1020, 'Pieces', '2019-04-25 00:46:14', '2019-04-25 00:54:41'),
(77, '123', 'iphone1', 2, 12, 10, 4777, 'KG', '2019-04-25 00:48:57', '2019-04-25 00:48:57'),
(78, '123', 'iphone2', 2, 12, 10, 4777, 'KG', '2019-04-25 00:50:55', '2019-04-25 00:50:55'),
(26, '007', 'iphone 123 X 3214', 5, 20, 20, 20000, 'pieces', '2019-04-11 01:37:51', '2019-04-11 01:37:51'),
(38, '007 jMS', 'myphone X', 5, 4, 2, 2000, 'litre', '2019-04-11 01:37:51', '2019-04-11 01:37:51');

-- --------------------------------------------------------

--
-- Table structure for table `purchasers`
--

CREATE TABLE `purchasers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double(8,2) DEFAULT NULL,
  `gst` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `purchasers`
--

INSERT INTO `purchasers` (`id`, `name`, `phone`, `amount`, `gst`, `created_at`, `updated_at`) VALUES
(2, 'Nitin', '6985741231', -999.32, 'sdrgsdrx', '2019-03-11 00:06:58', '2019-03-11 00:06:58'),
(3, 'sfd', '1234569870', 0.00, 'rdsf', '2019-03-11 23:09:54', '2019-03-11 23:09:54'),
(4, 'sdf', '1234569870', 100.00, 'werwe', '2019-03-15 06:26:45', '2019-03-15 06:26:45'),
(5, 'akgil', '123456789', -469996.24, '232612611151', '2019-04-10 02:10:54', '2019-04-10 02:10:54'),
(6, 'Akhil', '4792413336', 7120.00, 'zsfzsv', '2019-04-22 01:17:30', '2019-04-22 01:17:30'),
(7, 'xyz', '4792413336', 4673.36, '6', '2019-04-22 06:09:23', '2019-04-22 06:09:23');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_bills`
--

CREATE TABLE `purchase_bills` (
  `id` int(10) UNSIGNED NOT NULL,
  `bill_id` int(255) DEFAULT NULL,
  `bill_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cgstp` float DEFAULT NULL,
  `sgstp` float DEFAULT NULL,
  `product_rate` float DEFAULT NULL,
  `unit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cgst` double(8,2) DEFAULT NULL,
  `sgst` double(8,2) DEFAULT NULL,
  `quantity` double(8,2) DEFAULT NULL,
  `total` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `purchase_bills`
--

INSERT INTO `purchase_bills` (`id`, `bill_id`, `bill_no`, `product_code`, `product_name`, `cgstp`, `sgstp`, `product_rate`, `unit`, `cgst`, `sgst`, `quantity`, `total`, `created_at`, `updated_at`) VALUES
(1, 20190311, '1', 'sd', 'sd', 1, 1, 1, 'sd', 0.00, 0.00, 1.00, 0.99, '2019-03-11 03:10:44', '2019-03-11 03:10:44'),
(2, 20190311, '2', 'aaa', 'aa', 6, 6, 1000, 'aa', 60.00, 60.00, 1.00, 1120.00, '2019-03-11 04:47:57', '2019-03-11 04:47:57'),
(3, 20190312, '3', 'sdf', 'sdf', 6, 6, 1111, 'sdf', 66.66, 66.66, 1.00, 1244.32, '2019-03-12 11:39:58', '2019-03-12 11:39:58'),
(4, 20190312, '3', 'sdfx', 'sdf', 6, 6, 12, 'xvc', 0.72, 0.72, 1.00, 13.44, '2019-03-12 11:39:58', '2019-03-12 11:39:58'),
(5, 20190312, '3', 'sdf', 'sdf', 1, 1, 1, 'sdf', 0.01, 0.01, 1.00, 1.02, '2019-03-12 11:39:58', '2019-03-12 11:39:58'),
(6, 20190312, '3', '1ewds1', '12e', 1, 1, 1, 'ds', 0.11, 0.11, 11.00, 11.22, '2019-03-12 11:39:58', '2019-03-12 11:39:58'),
(7, 20190312, '3', '1', 'dcx', 1, 1, 1, 'cx', 0.01, 0.01, 1.00, 1.02, '2019-03-12 11:39:58', '2019-03-12 11:39:58'),
(8, 20190315, '4', 'cxv', 'sf', 6, 6, 1212, '0', 290.88, 290.88, 4.00, 5429.76, '2019-03-15 06:15:15', '2019-03-15 06:15:15'),
(9, 20190315, '5', 'asd', 'asd', 5, 5, 123, 'asd', 24.60, 24.60, 4.00, 541.20, '2019-03-15 06:18:18', '2019-03-15 06:18:18'),
(10, 20190315, '6', 'sd', 'zscv', 5, 5, 12, 'zsd', 0.60, 0.60, 1.00, 13.20, '2019-03-15 06:28:38', '2019-03-15 06:28:38'),
(11, 20190410, '7', '007', 'iphone 123 X 3214', 10, 10, 20000, 'pieces', 40000.00, 40000.00, 20.00, 476000.00, '2019-04-11 01:37:51', '2019-04-11 01:37:51'),
(12, 20190410, '7', '007 jMS', 'myphone X', 2, 2, 2, 'litre', 0.08, 0.08, 2.00, 3.76, '2019-04-11 01:37:51', '2019-04-11 01:37:51'),
(13, 1, '8', '1111', 'myphone X', 4, 4, 10000, 'pieces', 2000.00, 2000.00, 5.00, 54000.00, '2019-04-22 01:35:57', '2019-04-22 01:35:57'),
(14, 1, '8', '007 jMS', 'myphone XS', 2, 2, 1000, 'pieces', 60.00, 60.00, 3.00, 3120.00, '2019-04-22 01:35:57', '2019-04-22 01:35:57'),
(15, 2, '8', '1111', 'myphone X', 4, 4, 10000, 'pieces', 2000.00, 2000.00, 5.00, 54000.00, '2019-04-22 01:44:20', '2019-04-22 01:44:20'),
(16, 2, '8', '007 jMS', 'myphone XS', 2, 2, 1000, 'pieces', 60.00, 60.00, 3.00, 3120.00, '2019-04-22 01:44:20', '2019-04-22 01:44:20'),
(17, 3, '9', 'xyz01', 'XYZ -New', 2, 2, 5000, 'pieces', 500.00, 500.00, 5.00, 26000.00, '2019-04-22 06:10:18', '2019-04-22 06:10:18'),
(18, 3, '9', 'xyz02', 'XYZ-2NeW', 4, 4, 6000, 'pieces', 1440.00, 1440.00, 6.00, 38880.00, '2019-04-22 06:10:18', '2019-04-22 06:10:18'),
(19, 4, '10', '111', 'qqqq', 4, 4, 11111, 'pieces', 888.88, 888.88, 2.00, 23999.76, '2019-04-22 06:37:36', '2019-04-22 06:37:36'),
(20, 5, '11', '11', 'qwq', 2, 2, 2222, 'pieces', 88.88, 88.88, 2.00, 4621.76, '2019-04-22 06:40:09', '2019-04-22 06:40:09'),
(21, 6, '12', '11', 'eeee', 2, 2, 323, 'Sqft', 12.92, 12.92, 2.00, 671.84, '2019-04-22 06:42:25', '2019-04-22 06:42:25'),
(22, 7, '13', '123', 'iphone', 6, 6, 22777, 'KG', 13666.20, 13666.20, 10.00, 255102.40, '2019-04-24 06:59:24', '2019-04-24 06:59:24'),
(23, 8, '13', '123', 'iphone', 6, 6, 22777, 'KG', 13666.20, 13666.20, 10.00, 255102.40, '2019-04-24 07:01:08', '2019-04-24 07:01:08'),
(24, 9, '13', '123', 'iphone', 6, 6, 22777, 'KG', 13666.20, 13666.20, 10.00, 255102.40, '2019-04-24 07:01:20', '2019-04-24 07:01:20'),
(25, 10, '13', '123', 'iphone', 6, 6, 22777, 'KG', 13666.20, 13666.20, 10.00, 255102.40, '2019-04-24 07:01:43', '2019-04-24 07:01:43'),
(26, 11, '13', '123', 'iphone', 6, 6, 22777, 'KG', 13666.20, 13666.20, 10.00, 255102.40, '2019-04-24 07:02:14', '2019-04-24 07:02:14'),
(27, 12, '13', '123', 'iphone', 6, 6, 22777, 'KG', 13666.20, 13666.20, 10.00, 255102.40, '2019-04-24 07:02:50', '2019-04-24 07:02:50'),
(28, 13, '13', '123', 'iphone', 6, 6, 22777, 'KG', 13666.20, 13666.20, 10.00, 255102.40, '2019-04-24 07:04:45', '2019-04-24 07:04:45'),
(29, 14, '13', '123', 'iphone', 6, 6, 22777, 'KG', 13666.20, 13666.20, 10.00, 255102.40, '2019-04-24 07:05:35', '2019-04-24 07:05:35'),
(30, 15, '13', '123', 'iphone', 6, 6, 22777, 'KG', 13666.20, 13666.20, 10.00, 255102.40, '2019-04-24 07:06:59', '2019-04-24 07:06:59'),
(31, 16, '13', '123', 'iphone', 6, 6, 22777, 'KG', 13666.20, 13666.20, 10.00, 255102.40, '2019-04-24 07:09:55', '2019-04-24 07:09:55'),
(32, 17, '13', '123', 'iphone', 6, 6, 22777, 'KG', 13666.20, 13666.20, 10.00, 255102.40, '2019-04-24 07:10:03', '2019-04-24 07:10:03'),
(33, 18, '13', '123', 'iphone', 6, 6, 22777, 'KG', 13666.20, 13666.20, 10.00, 255102.40, '2019-04-24 07:11:06', '2019-04-24 07:11:06'),
(34, 19, '14', '123', 'iphone', 6, 6, 2777, 'KG', 1999.44, 1999.44, 12.00, 37322.88, '2019-04-24 07:12:51', '2019-04-24 07:12:51'),
(35, 19, '14', '122', 'ppo', 3, 3, 1000, 'Litre', 330.00, 330.00, 11.00, 11660.00, '2019-04-24 07:12:52', '2019-04-24 07:12:52'),
(37, 21, '15', '123', 'iphone', 6, 6, 3777, 'KG', 4532.40, 4532.40, 20.00, 84604.80, '2019-04-25 00:32:33', '2019-04-25 00:32:33'),
(38, 21, '15', '22', 'lol', 3, 3, 10000, 'Gram', 600.00, 600.00, 2.00, 21200.00, '2019-04-25 00:32:33', '2019-04-25 00:32:33'),
(62, 35, '16', '123', 'iphone', 6, 6, 2777, 'KG', 499.86, 499.86, 3.00, 9330.72, '2019-04-25 00:54:41', '2019-04-25 00:54:41'),
(61, 35, '16', '111', 'qqq', 3, 3, 1020, 'Pieces', 153.00, 153.00, 5.00, 5406.00, '2019-04-25 00:54:41', '2019-04-25 00:54:41'),
(60, 35, '16', '123', 'iphone', 6, 6, 4777, 'KG', 2866.20, 2866.20, 10.00, 53502.40, '2019-04-25 00:54:41', '2019-04-25 00:54:41');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_bill_details`
--

CREATE TABLE `purchase_bill_details` (
  `id` int(11) NOT NULL,
  `bill_date` date DEFAULT NULL,
  `bill_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purbillno` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purbilldate` date DEFAULT NULL,
  `purchaser_id` int(11) DEFAULT NULL,
  `tcgst` double(8,2) DEFAULT NULL,
  `tsgst` double(8,2) DEFAULT NULL,
  `discount` double(8,2) DEFAULT NULL,
  `total` double(8,2) DEFAULT NULL,
  `amountpay` double(8,2) DEFAULT NULL,
  `amountpayl` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_bill_details`
--

INSERT INTO `purchase_bill_details` (`id`, `bill_date`, `bill_no`, `purbillno`, `purbilldate`, `purchaser_id`, `tcgst`, `tsgst`, `discount`, `total`, `amountpay`, `amountpayl`, `created_at`, `updated_at`) VALUES
(1, '2019-04-22', '8', '646541', '2019-04-22', 6, 2060.00, 2060.00, NULL, 57120.00, 50000.00, 0.00, '2019-04-22 01:35:57', '2019-04-22 01:35:57'),
(2, '2019-04-22', '8', '646541', '2019-04-22', 6, 2060.00, 2060.00, NULL, 57120.00, 50000.00, 0.00, '2019-04-22 01:44:20', '2019-04-22 01:44:20'),
(3, '2019-04-22', '9', '987', '2019-04-22', 7, 1940.00, 1940.00, NULL, 64880.00, 60000.00, 0.00, '2019-04-22 06:10:17', '2019-04-22 06:10:17'),
(4, '2019-04-22', '10', '4575', '2019-04-22', 7, 888.88, 888.88, NULL, 23999.76, 24000.00, 0.00, '2019-04-22 06:37:36', '2019-04-22 06:37:36'),
(5, '2019-04-22', '11', 'qqq', '2019-04-22', 7, 88.88, 88.88, NULL, 4621.76, 4800.00, 0.00, '2019-04-22 06:40:09', '2019-04-22 06:40:09'),
(6, '2019-04-09', '12', 'ww', '2019-04-16', 7, 12.92, 12.92, NULL, 671.84, 700.00, 0.00, '2019-04-22 06:42:25', '2019-04-22 06:42:25'),
(7, '2019-04-24', '13', '111', '2019-04-24', 3, 13666.20, 13666.20, NULL, 255102.40, 255102.00, 0.00, '2019-04-24 06:59:24', '2019-04-24 06:59:24'),
(8, '2019-04-24', '13', '111', '2019-04-24', 3, 13666.20, 13666.20, NULL, 255102.40, 255102.00, 0.00, '2019-04-24 07:01:08', '2019-04-24 07:01:08'),
(9, '2019-04-24', '13', '111', '2019-04-24', 3, 13666.20, 13666.20, NULL, 255102.40, 255102.00, 0.00, '2019-04-24 07:01:20', '2019-04-24 07:01:20'),
(10, '2019-04-24', '13', '111', '2019-04-24', 3, 13666.20, 13666.20, NULL, 255102.40, 255102.00, 0.00, '2019-04-24 07:01:43', '2019-04-24 07:01:43'),
(11, '2019-04-24', '13', '111', '2019-04-24', 3, 13666.20, 13666.20, NULL, 255102.40, 255102.00, 0.00, '2019-04-24 07:02:14', '2019-04-24 07:02:14'),
(12, '2019-04-24', '13', '111', '2019-04-24', 3, 13666.20, 13666.20, NULL, 255102.40, 255102.00, 0.00, '2019-04-24 07:02:50', '2019-04-24 07:02:50'),
(13, '2019-04-24', '13', '111', '2019-04-24', 3, 13666.20, 13666.20, NULL, 255102.40, 255102.00, 0.00, '2019-04-24 07:04:44', '2019-04-24 07:04:44'),
(14, '2019-04-24', '13', '111', '2019-04-24', 2, 13666.20, 13666.20, NULL, 255102.40, 255102.00, 0.00, '2019-04-24 07:05:35', '2019-04-24 07:05:35'),
(15, '2019-04-24', '13', '111', '2019-04-24', 2, 13666.20, 13666.20, NULL, 255102.40, 255102.00, 0.00, '2019-04-24 07:06:59', '2019-04-24 07:06:59'),
(16, '2019-04-24', '13', '111', '2019-04-24', 2, 13666.20, 13666.20, NULL, 255102.40, 255102.00, 0.00, '2019-04-24 07:09:55', '2019-04-24 07:09:55'),
(17, '2019-04-24', '13', '111', '2019-04-24', 2, 13666.20, 13666.20, NULL, 255102.40, 255102.00, 0.00, '2019-04-24 07:10:02', '2019-04-24 07:10:02'),
(18, '2019-04-24', '13', '111', '2019-04-24', 2, 13666.20, 13666.20, NULL, 255102.40, 255102.00, 0.00, '2019-04-24 07:11:06', '2019-04-24 07:11:06'),
(19, '2019-04-24', '14', '121', '2019-04-24', 2, 2329.44, 2329.44, NULL, 48982.88, 48983.00, 0.00, '2019-04-24 07:12:51', '2019-04-24 07:12:51'),
(21, '2019-04-25', '15', '121', '2019-04-24', 2, 5132.40, 5132.40, NULL, 105804.80, 105805.00, 0.00, '2019-04-25 00:32:33', '2019-04-25 00:32:33'),
(35, '2019-04-25', '16', '111', '2019-04-25', 2, 3519.06, 3519.06, NULL, 68239.12, 69239.00, 0.00, '2019-04-25 00:54:41', '2019-04-25 00:54:41');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_bill_temps`
--

CREATE TABLE `purchase_bill_temps` (
  `id` int(10) UNSIGNED NOT NULL,
  `bill_date` date DEFAULT NULL,
  `bill_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purbillno` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purbilldate` date DEFAULT NULL,
  `purchaser_id` int(11) DEFAULT NULL,
  `product_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cgstp` float DEFAULT NULL,
  `sgstp` float DEFAULT NULL,
  `product_rate` float DEFAULT NULL,
  `unit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cgst` double(8,2) DEFAULT NULL,
  `sgst` double(8,2) DEFAULT NULL,
  `quantity` double(8,2) DEFAULT NULL,
  `discount` double(8,2) DEFAULT NULL,
  `total` double(8,2) DEFAULT NULL,
  `amountpay` double(8,2) DEFAULT NULL,
  `amountpayl` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_payments`
--

CREATE TABLE `purchase_payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `billno` int(11) DEFAULT NULL,
  `desc` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billdate` date DEFAULT NULL,
  `mode` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ref` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `purchase_payments`
--

INSERT INTO `purchase_payments` (`id`, `billno`, `desc`, `billdate`, `mode`, `ref`, `amount`, `created_at`, `updated_at`) VALUES
(1, 1, '', '2019-03-11', '', '', 11.00, '2019-03-11 03:10:44', '2019-03-11 03:10:44'),
(2, 2, '', '2019-03-11', '', '', 240.00, '2019-03-11 04:47:57', '2019-03-11 04:47:57'),
(3, 3, '', '2019-03-12', '', '', 0.00, '2019-03-12 11:39:58', '2019-03-12 11:39:58'),
(4, 4, '', '2019-03-15', '', '', 2323.00, '2019-03-15 06:15:16', '2019-03-15 06:15:16'),
(5, 5, '', '2019-03-15', '', '', 212.00, '2019-03-15 06:18:18', '2019-03-15 06:18:18'),
(6, 6, '', '2019-03-15', '', '', 2323.00, '2019-03-15 06:28:38', '2019-03-15 06:28:38'),
(7, 7, 'Bill', '2019-04-10', '', '', 470000.00, '2019-04-11 01:37:51', '2019-04-11 01:37:51'),
(8, 8, 'Bill', '2019-04-22', 'cash', 'cash', 50000.00, '2019-04-22 01:44:20', '2019-04-22 01:44:20'),
(9, 9, 'Bill', '2019-04-22', 'cash', 'cash', 60000.00, '2019-04-22 06:10:18', '2019-04-22 06:10:18'),
(10, 10, 'Bill', '2019-04-22', 'cash', 'cash', 24000.00, '2019-04-22 06:37:36', '2019-04-22 06:37:36'),
(11, 11, 'Bill', '2019-04-22', 'cash', 'cash', 4800.00, '2019-04-22 06:40:09', '2019-04-22 06:40:09'),
(12, 12, 'Bill', '2019-04-09', 'cash', 'cash', 700.00, '2019-04-22 06:42:25', '2019-04-22 06:42:25'),
(13, 13, 'Bill', '2019-04-24', 'cash', 'cash', 255102.00, '2019-04-24 07:11:06', '2019-04-24 07:11:06'),
(14, 14, 'Bill', '2019-04-24', 'cash', 'cash', 48983.00, '2019-04-24 07:12:52', '2019-04-24 07:12:52'),
(15, 15, 'Bill', '2019-04-25', 'cash', 'cash', 105805.00, '2019-04-25 00:32:33', '2019-04-25 00:32:33'),
(16, 16, 'Bill', '2019-04-25', 'cash', 'cash', 68239.00, '2019-04-25 00:39:57', '2019-04-25 00:39:57'),
(17, 16, 'Bill', '2019-04-25', 'cash', 'cash', 68239.00, '2019-04-25 00:46:14', '2019-04-25 00:46:14'),
(18, 16, 'Bill', '2019-04-25', 'cash', 'cash', 68239.00, '2019-04-25 00:48:57', '2019-04-25 00:48:57'),
(19, 16, 'Bill', '2019-04-25', 'cash', 'cash', 68239.00, '2019-04-25 00:51:58', '2019-04-25 00:51:58'),
(20, 16, 'Bill', '2019-04-25', 'cash', 'cash', 69239.00, '2019-04-25 00:54:41', '2019-04-25 00:54:41');

-- --------------------------------------------------------

--
-- Table structure for table `sales_bills`
--

CREATE TABLE `sales_bills` (
  `id` int(10) UNSIGNED NOT NULL,
  `bill_id` int(11) DEFAULT NULL,
  `bill_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `rate` float DEFAULT NULL,
  `quantity` float DEFAULT NULL,
  `total` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sales_bills`
--

INSERT INTO `sales_bills` (`id`, `bill_id`, `bill_no`, `product_id`, `rate`, `quantity`, `total`, `created_at`, `updated_at`) VALUES
(1, 20190309, '1', 1, NULL, 1, 10600, '2019-03-09 01:38:26', '2019-03-09 01:38:26'),
(2, 20190309, '2', 2, NULL, 2, 21200, '2019-03-09 02:29:44', '2019-03-09 02:29:44'),
(3, 20190312, '3', 1, NULL, 1, 10600, '2019-03-12 11:58:02', '2019-03-12 11:58:02'),
(4, 20190302, '4', 1, 18800, 1, 20000, '2019-03-22 06:07:08', '2019-03-22 06:07:08'),
(5, 20190302, '4', 1, 18800, 2, 40000, '2019-03-22 06:07:08', '2019-03-22 06:07:08'),
(6, 4, '5', 1, 14100, 1, 15000, '2019-04-22 05:42:26', '2019-04-22 05:42:26'),
(7, 4, '5', 1, 9400, 1, 10000, '2019-04-22 05:42:26', '2019-04-22 05:42:26'),
(8, 4, '5', 1, 112800, 1, 120000, '2019-04-22 05:42:26', '2019-04-22 05:42:26'),
(9, 4, '5', 2, 9400, 1, 10000, '2019-04-22 05:42:26', '2019-04-22 05:42:26'),
(10, 4, '5', 8, 99960, 2, 204000, '2019-04-22 05:42:26', '2019-04-22 05:42:26'),
(11, 4, '5', 2, 1880, 1, 2000, '2019-04-22 05:42:26', '2019-04-22 05:42:26'),
(12, 4, '5', 1, 13160, 2, 28000, '2019-04-22 05:42:26', '2019-04-22 05:42:26'),
(13, 4, '5', 1, 13160, 1, 14000, '2019-04-22 05:42:26', '2019-04-22 05:42:26'),
(14, 4, '5', 1, 13160, 1, 14000, '2019-04-22 05:42:26', '2019-04-22 05:42:26'),
(15, 4, '5', 1, 112800, 1, 120000, '2019-04-22 05:42:26', '2019-04-22 05:42:26'),
(16, 4, '5', 1, 8800, 1, 10000, '2019-04-22 05:42:26', '2019-04-22 05:42:26'),
(17, 4, '5', 1, 26400, 5, 150000, '2019-04-22 05:42:26', '2019-04-22 05:42:26'),
(18, 4, '5', 1, 26400, 1, 30000, '2019-04-22 05:42:26', '2019-04-22 05:42:26'),
(19, 4, '5', 1, 8800, 1, 10000, '2019-04-22 05:42:26', '2019-04-22 05:42:26'),
(20, 4, '5', 2, 10340, 1, 11000, '2019-04-22 05:42:26', '2019-04-22 05:42:26'),
(21, 5, '6', 69, 384, 1, 400, '2019-04-22 07:08:23', '2019-04-22 07:08:23');

-- --------------------------------------------------------

--
-- Table structure for table `sales_bill_details`
--

CREATE TABLE `sales_bill_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `bill_date` date DEFAULT NULL,
  `bill_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `tcgst` double(8,2) DEFAULT NULL,
  `tsgst` double(8,2) DEFAULT NULL,
  `total` double(8,2) DEFAULT NULL,
  `amountpay` double(8,2) DEFAULT NULL,
  `amountpayl` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sales_bill_details`
--

INSERT INTO `sales_bill_details` (`id`, `bill_date`, `bill_no`, `customer_id`, `tcgst`, `tsgst`, `total`, `amountpay`, `amountpayl`, `created_at`, `updated_at`) VALUES
(1, '2019-04-18', '11', 2, 450.00, 450.00, 15000.00, 15000.00, 100.00, '2019-04-18 03:15:48', '2019-04-18 03:15:48'),
(2, '2019-04-18', '12', 2, 1050.00, 1050.00, 35000.00, 35000.00, 200.00, '2019-04-18 04:53:04', '2019-04-18 04:53:04'),
(3, '2019-04-03', '5', 2, 24360.00, 24360.00, 748000.00, 750000.00, 0.00, '2019-04-22 05:40:45', '2019-04-22 05:40:45'),
(4, '2019-04-03', '5', 2, 24360.00, 24360.00, 748000.00, 750000.00, 0.00, '2019-04-22 05:42:26', '2019-04-22 05:42:26'),
(5, '2019-04-22', '6', 7, 8.00, 8.00, 400.00, 400.00, 0.00, '2019-04-22 07:08:23', '2019-04-22 07:08:23');

-- --------------------------------------------------------

--
-- Table structure for table `sales_bill_temps`
--

CREATE TABLE `sales_bill_temps` (
  `id` int(10) UNSIGNED NOT NULL,
  `bill_date` date DEFAULT NULL,
  `bill_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `rate` float DEFAULT NULL,
  `cgst` float DEFAULT NULL,
  `sgst` float DEFAULT NULL,
  `quantity` float DEFAULT NULL,
  `discount` float DEFAULT NULL,
  `total` float DEFAULT NULL,
  `amountpay` float DEFAULT NULL,
  `amountpayl` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sales_bill_temps`
--

INSERT INTO `sales_bill_temps` (`id`, `bill_date`, `bill_no`, `customer_id`, `product_id`, `rate`, `cgst`, `sgst`, `quantity`, `discount`, `total`, `amountpay`, `amountpayl`, `created_at`, `updated_at`) VALUES
(1, '2019-04-24', '7', 8, 9, 9800, 100, 100, 1, NULL, 10000, NULL, NULL, '2019-04-24 01:34:02', '2019-04-24 01:34:02'),
(2, '2019-04-24', '7', 8, 37, 20000, 2500, 2500, 1, NULL, 25000, NULL, NULL, '2019-04-24 01:34:21', '2019-04-24 01:34:21');

-- --------------------------------------------------------

--
-- Table structure for table `sales_payments`
--

CREATE TABLE `sales_payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `billno` int(11) DEFAULT NULL,
  `desc` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billdate` date DEFAULT NULL,
  `mode` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ref` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sales_payments`
--

INSERT INTO `sales_payments` (`id`, `billno`, `desc`, `billdate`, `mode`, `ref`, `amount`, `created_at`, `updated_at`) VALUES
(1, 3, '', '2019-03-12', '', '', 11200.00, '2019-03-12 11:58:02', '2019-03-12 11:58:02'),
(2, 4, '', '2019-03-02', '', '', 30000.00, '2019-03-22 06:07:08', '2019-03-22 06:07:08'),
(3, 5, 'Bill', '2019-04-03', 'cash', 'cash', 750000.00, '2019-04-22 05:42:26', '2019-04-22 05:42:26'),
(4, 6, 'Bill', '2019-04-22', 'cash', 'cash', 400.00, '2019-04-22 07:08:23', '2019-04-22 07:08:23');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `role`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin', 0, '$2y$10$HjlwUNi/s8bpXh09PCZ68OIQAnD4h4ykpEQACDw0WrEIW3tFZll1W', NULL, '2019-02-27 05:34:45', '2019-02-27 05:34:45');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_nums`
--
ALTER TABLE `invoice_nums`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `open_closes`
--
ALTER TABLE `open_closes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchasers`
--
ALTER TABLE `purchasers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_bills`
--
ALTER TABLE `purchase_bills`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_bill_details`
--
ALTER TABLE `purchase_bill_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_bill_temps`
--
ALTER TABLE `purchase_bill_temps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_payments`
--
ALTER TABLE `purchase_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_bills`
--
ALTER TABLE `sales_bills`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_bill_details`
--
ALTER TABLE `sales_bill_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_bill_temps`
--
ALTER TABLE `sales_bill_temps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_payments`
--
ALTER TABLE `sales_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `invoice_nums`
--
ALTER TABLE `invoice_nums`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `open_closes`
--
ALTER TABLE `open_closes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT for table `purchasers`
--
ALTER TABLE `purchasers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `purchase_bills`
--
ALTER TABLE `purchase_bills`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `purchase_bill_details`
--
ALTER TABLE `purchase_bill_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `purchase_bill_temps`
--
ALTER TABLE `purchase_bill_temps`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `purchase_payments`
--
ALTER TABLE `purchase_payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `sales_bills`
--
ALTER TABLE `sales_bills`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `sales_bill_details`
--
ALTER TABLE `sales_bill_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sales_bill_temps`
--
ALTER TABLE `sales_bill_temps`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sales_payments`
--
ALTER TABLE `sales_payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
