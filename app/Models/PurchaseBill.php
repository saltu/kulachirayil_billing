<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PurchaseBill extends Model
{
   protected $fillable = [
        'bill_id', 'bill_no', 'product_code','product_name', 'cgstp', 'sgstp', 'product_rate','cgst','sgst', 'unit','quantity','total'
    ];

    protected $hidden = [];
}
