<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PurchaseBillDetail extends Model
{
    protected $fillable = [
        'bill_date', 'bill_no', 'purbillno',  'purbilldate',  'purchaser_id', 'tcgst', 'tsgst', 'discount','total','amountpay','amountpayl'
    ];

    protected $hidden = [];
}
