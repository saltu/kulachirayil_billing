<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalesPayment extends Model
{
    protected $fillable = [
        'bill_no','desc', 'billdate', 'amount'
    ];

    protected $hidden = [];
}
