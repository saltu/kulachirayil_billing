<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\OpenClose;
use DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function index(){
    	$date = date('Y-m-d');
	    $status = 0;
	    $amount = 0;
	    $acc = DB::table ('accounts')->select ('*')->where ('id' , '=' , 1)->get ();
	    
	    foreach ($acc as $key => $value) {
	        $amount = $value->amount;
	    }

	    $acc1 = DB::table ('open_closes')->select ('*')->where ('current_date' , '=' , $date)->get ();

	    foreach ($acc1 as $key => $value) {
	            $status = $value->id;
	        }

	    if($status == 0)
	    {
	        $open = new OpenClose();
	        $open->current_date = $date;
	        $open->opening = $amount;
	        $open->save();
	    }
	    return view('welcome');
    }
}
