<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PurchaseBillDetail;
use App\Models\PurchasePayment;
use Auth;
use DB;
use Session;
use Hash;
use Redirect;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class PurchasePaymentController extends Controller
{
	public function index(){
    	return view('payment.purchase');
    }

    public function billno(Request $request){
    	
		$json=array();

		$status = PurchaseBillDetail::where('bill_no','like', '%' . $request->term . '%')
						->groupBy('bill_no')
						->get();

		foreach ($status as $key => $value) {
				$json[] =  $value->bill_no;
			}

		echo json_encode($json);						
	}

	public function billdetails(Request $request){
		$bill = PurchaseBillDetail::where('bill_no',[$request->value])
				->get();
		
		$tot = 0;

		foreach ($bill as $key => $value) {
				$purname = DB::table('purchasers')
						->select('name')
						->where('id', $value->purchaser_id)	
						->get();

				$tot +=  $value->total;	
				$json['amountpay'] =  $value->amountpay;
				$json['amountpayl'] =  $value->amountpayl;
				$json['ppurid'] = $value->purchaser_id;
			}

		$json['ppurname'] =  $purname[0]->name;
		$json['ppurchasetot'] =  round($tot);

		echo json_encode($json);
	}

	public function billadd(Request $request){

    	// echo $request->code.$request->name.$request->purchased.$request->gst.$request->quantity.$request->rate;die;
		$purchasepayment = new PurchasePayment();
		$purchasepayment->billno = $request->ppbillno;
		$purchasepayment->desc = "Payment";
		$purchasepayment->billdate = date('y-m-d');
		$purchasepayment->amount = $request->ppurchasepay;
		$purchasepayment->mode = "cash";
		$purchasepayment->ref = "cash";
		$purchasepayment->save ();


		DB::update("UPDATE purchase_bills SET amountpayl = amountpayl + '$request->ppurchasepay' where bill_no = '$request->ppbillno'");

		DB::update("UPDATE accounts SET amount = amount - '$request->ppurchasepay' where id = 1");
		DB::update("UPDATE accounts SET amount = amount + '$request->ppurchasepay' where id = 3");

		DB::update("UPDATE purchasers SET amount = amount + '$request->ppurchasepay' where id = '$request->ppurid'");

    	return redirect('home');
    	   	
    }


    public function purchaser(){
    	return view('payment.purchaser');
    }

    public function purcbillno(Request $request){
    	// echo $request->value;die;

    	$json=array();

		$status = PurchaseBillDetail::where('purchaser_id',$request->value)
						->groupBy('bill_no')
						->get();

		foreach ($status as $key => $value) {
				$json[] =  $value->bill_no;
			}

		echo json_encode($json);
    }
}
