<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\InvoiceNum;
use App\Models\Purchaser;
use App\Models\PurchaseBill;
use App\Models\PurchaseBillDetail;
use App\Models\PurchaseBillTemp;
use App\Models\PurchasePayment;
use App\Models\HsnCode;
use Auth;
use DB;
use Session;
use Hash;
use Redirect;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class PurchaseController extends Controller
{
     public function prodcodename(Request $request){
        
		$json=array();
		$status = HsnCode::where('name','like', '%' . $request->term . '%')
						->get();
        
		foreach ($status as $key => $value) {
				$val = $value->name;
				$json[] =  $val;
			}
		echo json_encode($json);
	}
    
     public function prodcode(Request $request){
        
		$json=array();
		$status = HsnCode::where('hsncode','like', '%' . $request->term . '%')
						->get();
        
		foreach ($status as $key => $value) {
				$val = $value->hsncode;
				$json[] =  $val;
			}
		echo json_encode($json);
	}
 
//	public function codedetails(Request $request){
//		$cust = HsnCode::where('hsncode',$request->value)
//				->get();
//        echo print_r($cust[0]->name);die;
//        $json = array();
//		foreach ($cust as $key => $value) {
//				$json[] .= $value->name;
//			}
//		echo json_encode($json);die;
//	}

 	public function codenamedetails(Request $request){

		$cust = HsnCode::where('name',$request->value)
				->get();

        $json = array();
		foreach ($cust as $key => $value) {
                $json[] = (['name'=>$value->name , 'hsncode'=>$value->hsncode, 'gstrate'=> $value->gstrate ]);
			}
        
		echo json_encode($json);die;
	}   
    
	public function codedetails(Request $request){

		$cust = HsnCode::where('hsncode',$request->value)
				->get();

        $json = array();
		foreach ($cust as $key => $value) {
                $json[] = (['name'=>$value->name , 'hsncode'=>$value->hsncode, 'gstrate'=> $value->gstrate ]);
			}
        
		echo json_encode($json);die;
	}
    

    
    public function purchaseadd(){
    	$invoice = DB::table('invoice_nums')
						->select('invoice_num')
						->where('id','2')
						->where('name','Purchase')
						->get();
		$innum = $invoice[0]->invoice_num;

		$purchase_bill = DB::table('purchase_bill_temps')
						->select('*')
						->get();

		$purchase_bill_temps = DB::table('purchase_bill_temps')
						->select('*')
						->get();

    	return view('purchase.purchaseadd', compact('innum', 'purchase_bill', 'purchase_bill_temps'));
    }
    
    public function tempadd(Request $request){
    	// echo print_r($request->all());die;
    	$purid = $request->purid;
    	if($request->purid == ''){
    		$purchaser = new Purchaser();
    		$purchaser->name = $request->purname;
    		$purchaser->phone = $request->purphone;
    		$purchaser->gst = $request->gst;
    		$purchaser->amount = 0;
    		$purchaser->save();
    		$purid = $purchaser->id;
    	}
		$purchase = new PurchaseBillTemp();
		$purchase->bill_date = $request->pudate;
		$purchase->bill_no = $request->billno;
		$purchase->purchaser_id = $purid;
		$purchase->product_code = $request->item_code;
		$purchase->product_name = $request->item;
		$purchase->cgstp = $request->item_cgstp;
		$purchase->sgstp = $request->item_sgstp;
		$purchase->product_rate = $request->item_rate;
		$purchase->unit = $request->unit;
		$purchase->quantity = $request->item_qty;
		$purchase->discount = 0;
		$purchase->cgst = $request->item_cgst;
		$purchase->sgst = $request->item_sgst;
		$purchase->total = $request->item_total;
		$purchase->purbillno = $request->purbillno;
		$purchase->purbilldate = $request->purbilldate;
		$purchase->save ();

    	return response()->json(['success'=>'Data is successfully added']); 
    }

    
    public function tempdetails(Request $request){

		$purchase_bill = DB::table('purchase_bill_temps')
						->select('*')
						->get();

		$purchase_bill_temps = DB::table('purchase_bill_temps')
						->select('*')
						->get();

    	return view('purchase.purchasedetails', compact('purchase_bill', 'purchase_bill_temps'));
    }
    public function tempdelete(){

    	PurchaseBillTemp::truncate();

    	return redirect('purchase/purchaseadd');
    }

    public function tempdeletedata(Request $request){

     	// echo $request->id;die;
		$product = PurchaseBillTemp::findorfail($request->id);
		$product->destroy($request->id);

		return response()->json(['success'=>'Data is successfully Deleted']);     	
    }

//$purchase->unit = $value->unit;
    
    public function purchaseaddmain(Request $request){

    	$tot = 0;
    	$tcgst = 0;
    	$tsgst = 0;
    	$bill_date = '';
    	$bill_no = '';
    	$purchaser_id = 0;
    	$amountpay = $request->amountpay;
    	$discount = 0;
    	$purbilldate = '';
    	$purbillno = '';
    	$mode = $_POST['pmode'];

    	DB::update("UPDATE purchase_bill_temps SET amountpay = '$request->amountpay'");
        $purchase_bill_temps = DB::table('purchase_bill_temps')
						->select('*')
						->get();
		foreach ($purchase_bill_temps as $key => $value) {
			$tot += $value->total;
			$tcgst += $value->cgst;
			$tsgst += $value->sgst;	
			$discount = $value->discount;			
			$bill_date = $value->bill_date;
	    	$bill_no = $value->bill_no;
	    	$purchaser_id = $value->purchaser_id;
	    	$purbilldate = $value->purbilldate;
    		$purbillno = $value->purbillno;
		}

		$purchasedet = new PurchaseBillDetail();

		$purchasedet->bill_date = $bill_date;
		$purchasedet->bill_no = $bill_no;
		$purchasedet->purchaser_id = $purchaser_id;
		$purchasedet->tcgst = $tcgst;
		$purchasedet->tsgst = $tsgst;
		$purchasedet->total = $tot;
		$purchasedet->discount = $discount;
		$purchasedet->amountpay = $amountpay;
		$purchasedet->amountpayl = 0;
		$purchasedet->purbillno = $purbillno;
		$purchasedet->purbilldate = $purbilldate;
	
		$purchasedet->save ();

		foreach ($purchase_bill_temps as $key => $value) {
			$purchase = new PurchaseBill();

			$purchase->bill_id = $purchasedet->id;
			$purchase->bill_no = $value->bill_no;
			$purchase->product_code = $value->product_code;
			$purchase->product_name = $value->product_name;
			$purchase->cgstp = $value->cgstp;
			$purchase->sgstp = $value->sgstp;
			$purchase->product_rate = $value->product_rate;
            $purchase->unit = $value->unit;
			$purchase->quantity = $value->quantity;
			$purchase->cgst = $value->cgst;
			$purchase->sgst = $value->sgst;
			$purchase->total = $value->total;

			$purchase->save ();

			$prodet = Product::where('name',$purchase->product_name)
								->where('code',$purchase->product_code)
								->where('purchased',$purchasedet->purchaser_id)
								->where('rate',$purchase->product_rate)
								->first();
			// echo $prodet;die;
			// echo $purchase->product_name.$purchase->product_code.$purchasedet->purchaser_id.$prodet->id;die;
			if($prodet == ''){
				$name = '';
				$prodetname = Product::where('name',$purchase->product_name)
								->first();
				if($prodetname != ''){
					$name = $purchase->product_name.mt_rand(0, 20);
				} else {
					$name = $purchase->product_name;
				}


	            $product = new Product();
	            $product->code = $purchase->product_code;
	            $product->name = $name;
	            $product->purchased = $purchasedet->purchaser_id;
	            $product->gst = $purchase->cgstp+$purchase->sgstp;
	            $product->quantity = $purchase->quantity;
	            $product->rate = $purchase->product_rate;
	            $product->unit = $purchase->unit;

	            $product->save ();

			} else {
				$product = Product::find($prodet->id);
	            $product->code = $purchase->product_code;
	            $product->name = $purchase->product_name;
	            $product->purchased = $purchasedet->purchaser_id;
	            $product->gst = $purchase->cgstp+$purchase->sgstp;
	            $product->quantity = $prodet->quantity + $purchase->quantity;
	            $product->rate = $purchase->product_rate;
	            $product->unit = $purchase->unit;

	            $product->save ();
			}

		}

		if($mode == 'cash'){
			$purchasepayment = new PurchasePayment();
			$purchasepayment->billno = $purchasedet->bill_no;
			$purchasepayment->desc = "Bill";
			$purchasepayment->billdate = $purchasedet->bill_date;
			$purchasepayment->amount = $request->amountpay;
			$purchasepayment->mode = "cash";
			$purchasepayment->ref = "cash";
			$purchasepayment->save ();
		} else if($mode == 'bank') {
			$purchasepayment = new PurchasePayment();
			$purchasepayment->billno = $purchasedet->bill_no;
			$purchasepayment->desc = "Bill";
			$purchasepayment->billdate = $purchasedet->bill_date;
			$purchasepayment->amount = $request->amountpay;
			$purchasepayment->mode = "cash";
			$purchasepayment->ref = $request->pref;
			$purchasepayment->save ();
		}

			$amnt = $tot - $request->amountpay;

		DB::update("UPDATE invoice_nums SET invoice_num = invoice_num + 1 where id = 2");
		DB::update("UPDATE accounts SET amount = amount - '$request->amountpay' where id = 1");
		DB::update("UPDATE accounts SET amount = amount + '$request->amountpay' where id = 3");

		DB::update("UPDATE purchasers SET amount = amount + '$amnt' where id = '$purchaser_id'");
		PurchaseBillTemp::truncate();

    	return redirect('purchase/purchaseadd');
    }

    public function purchasereport(){
    	return view('purchase.purchasereport');
    }
 
    public function purchasereportdata(Request $request){
		// echo $request->datefrom.$request->dateto;die;
		$datefrom = $request->datefrom;
		$dateto = $request->dateto;

		$purchase_bill_data = DB::table('purchase_bill_details')
										->select('*')
										->whereBetween('bill_date',[$datefrom,$dateto])
										->groupBy('bill_no')
										->orderBy('id','DESC')
										->get();

		return view('purchase.purchasereportlist', compact('purchase_bill_data'));
	}
	public function purchasereportdetails($billno){
		// echo $billno;die;

		$purchase_bill = DB::table('purchase_bill_details')
												->select('*')
												->where('bill_no',$billno)
												->get();

		$purchase_bill_temps = DB::table('purchase_bills')
														->select('*')
														->where('bill_no',$billno)
														->get();

		return view('purchase.purchasereportdetails', compact('purchase_bill', 'purchase_bill_temps'));
	} 
}
