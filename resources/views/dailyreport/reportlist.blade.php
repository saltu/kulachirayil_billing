<script src="{{ asset('resources/assets/pages/scripts/table-datatables-editable-dailyreport.js') }}" type="text/javascript"></script>
<table class="table table-striped table-hover table-bordered" id="sample_editable_1_dailyreport">
    <thead>
        <tr class="d-flex">
            <th class="text-center"> Title </th>
            <th class="text-center"> Bill No </th>
            <th class="text-center"> Holder Name </th>
            <th class="text-center"> Amount Payed </th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $stot = 0;
            $ptot = 0;
            $etot = 0;
        ?>
       <!-- opeinig balance -->
        @if(count($opening) > 0)
        <tr>
            <td> Opening Balance </td>
            <td colspan="2" class="text-center"> -- </td>
            <td>{{ $opening[0]->opening }}</td>
        </tr>
        @endif
        <!-- sales -->
        @if(count($sales_bills) > 0)
            @foreach($sales_bills as $sales_bill)
                <tr>
                    <td> Sales {{ $sales_bill->desc }} </td>
                    <?php
                        $sales_billss = DB::table('sales_bill_details')
                                    ->select('*')
                                    ->where('bill_no',$sales_bill->billno)
                                    ->get();
                        $customers = DB::table('customers')
                                    ->select('*')
                                    ->where('id',$sales_billss[0]->customer_id)
                                    ->get();
                    ?>
                    <td> {{ $sales_bill->billno }} </td>
                    <td> {{ $customers[0]->name }} </td>
                    <td> {{ $sales_bill->amount }} </td>
                    <?php $stot += $sales_bill->amount; ?>
                </tr>
            @endforeach
        @endif
        <!-- purchase -->
        @if(count($purchase_bills) > 0)
            @foreach($purchase_bills as $purchase_bill)
                <tr>
                    <td> Purchase {{ $purchase_bill->desc }} </td>
                    <?php 
                        $purchase_billss = DB::table('purchase_bill_details')
                                    ->select('*')
                                    ->where('bill_no',$purchase_bill->billno)
                                    ->get();
                        $purchasers = DB::table('purchasers')
                                    ->select('*')
                                    ->where('id',$purchase_billss[0]->purchaser_id)
                                    ->get();

                    ?>
                    <td> {{ $purchase_bill->billno }} </td>
                    <td> {{ $purchasers[0]->name }} </td>
                    <td> {{ $purchase_bill->amount }} </td>
                    <?php $ptot += $purchase_bill->amount; ?>
                </tr>
            @endforeach
        @endif 
        <!-- expense -->
        @if(count($expenses) > 0)
            @foreach($expenses as $expense)
                <tr>
                    <td> Expense </td>
                    <td colspan="2" class="text-center"> {{ $expense->description }} </td>
                    <td> {{ $expense->amount }} </td>
                    <?php $etot += $expense->amount; ?>
                </tr>
            @endforeach
        @endif 
        <tr>
            <td colspan="2" class="text-right">Opening Balance</td>
            <td colspan="2">{{ $opening[0]->opening }}</td>
        </tr>
        <tr>
            <td colspan="2" class="text-right">Sales Total</td>
            <td colspan="2">{{ $stot }}</td>
        </tr>
        <tr>
            <td colspan="2" class="text-right">Purchase Total</td>
            <td colspan="2">{{ $ptot }}</td>
        </tr>
        <tr>
            <td colspan="2" class="text-right">Expense Total</td>
            <td colspan="2">{{ $etot }}</td>
        </tr>
        <tr>
            <?php 
            $op = $opening[0]->opening;
            $tot = ($op + $stot) - ($ptot + $etot);
            ?>
            <td colspan="2" class="text-right">Account Balance</td>
            <td colspan="2">{{ $tot }}</td>
        </tr>
    </tbody>
</table>