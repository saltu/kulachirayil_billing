@extends('layouts.app')
@section('content')
<!-- END HEADER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Sale</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{ route('home') }}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="#">Sales</a>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->
            @if(count($sales_bill) > 0)
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i>
                                    </div>
                                    <!-- <div class="tools">
                                        <a href="javascript:;" class="collapse"> </a>
                                        <a href="javascript:;" class="reload"> </a>
                                    </div> -->
                                </div>
                                <div class="portlet-body"> 
                                    <!-- BEGIN FORM-->
                                    <form action="{{ route('sales.temp.details') }}" method="POST" class="form-horizontal">
                                        {{ csrf_field() }}
                                        <div class="form-body">
                                            <h3 class="form-section">Info</h3>
                                            <div class="row">                                                
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Date</label>
                                                        <div class="col-md-9">
                                                            <input type="date" class="form-control" id="cudate" name="cudate" required value="{{ $sales_bill[0]->bill_date }}" placeholder="Dadte">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                
                                                <!-- Bill number automatic -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Bill</label>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control" id="billno" name="billno" readonly value="{{ $sales_bill[0]->bill_no }}" placeholder="Automatic">
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <!-- Name -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Name</label>
                                                        <?php 
                                                            $sales_bill_temps_va = DB::table('customers')
                                                                        ->select('*')
                                                                        ->where('id',$sales_bill[0]->customer_id)
                                                                        ->get();
                                                        ?>
                                                        <input type="hidden" value="{{ $sales_bill_temps_va[0]->id }}" class="form-control custid" id="custid" name="custid">
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control custname" id="custname" name="custname" value="{{ $sales_bill_temps_va[0]->name }}" placeholder="Customer name">
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <!-- phone number  -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Phone</label>
                                                        <div class="col-md-9">
                                                            <input type="number" value="{{ $sales_bill_temps_va[0]->phone }}" class="form-control phone" id="phone" name="phone" placeholder="Phone number ">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->                                           
                                            <br>
                                            <?php
                                                $cgst = 0;
                                                $sgst = 0;
                                                $total = 0;
                                                $dis = 0;
                                                $dis_am = 0;
                                                $gran_tot = 0;
                                                foreach($sales_bill as $sales_bil){
                                                    $cgst += $sales_bil->cgst;
                                                    $sgst += $sales_bil->sgst;
                                                    $dis += $sales_bil->discount;
                                                    $total += $sales_bil->total;
                                                }

                                                    $dis_am = $total * ($dis/100);
                                                    $gran_tot = ($total + $cgst + $sgst) - $dis;
                                            ?>
                                            <div class="row">                 
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">CGST</label>
                                                        <div class="col-md-9">
                                                            <input type="text" readonly value="{{ $cgst }}" class="form-control" id="subcgst" name="subcgst">
                                                            <input type="hidden" readonly value="{{ $cgst }}" class="form-control" id="subcgsttem" name="subcgsttem">
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">SGST</label>
                                                        <div class="col-md-9">
                                                            <input type="text" readonly value="{{ $sgst }}" class="form-control" id="subsgst" name="subsgst">
                                                            <input type="hidden" readonly value="{{ $cgst }}" class="form-control" id="subsgsttem" name="subsgsttem">
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">SubTotal</label>
                                                        <div class="col-md-9">
                                                            <input type="text" readonly value="{{ $total }}" class="form-control" id="subtotal" name="subtotal">
                                                            <input type="hidden" readonly value="{{ $total }}" class="form-control" id="subtotaltem" name="subtotaltem">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                            
                                            <h3 class="form-section">Items</h3>
                                            <!--/row-->
                                            <div class="row" >
                                                <div class="col-md-12">
                                                    <div class="portlet-body">
                                                        <div class="table-responsive" style="height:120px !important;" >
                                                            <table class="table table-striped table-bordered table-hover">
                                                                <thead>
                                                                    <tr class="d-flex" >
                                                                        <th class ="text-center col-md-2"> Item </th>
                                                                        <th class ="text-center  col-md-1"> Code </th>
                                                                        <th class="text-center"> ORate </th>
                                                                        <th class="text-center"> PRate </th>
                                                                        <th class="text-center"> Rate </th>
                                                                        <th class="text-center"> Unit </th>
                                                                        <th class="text-center"> Qty </th>
                                                                        <th class="text-center"> CGST % </th>
                                                                        <th class="text-center"> CGST </th>
                                                                        <th class="text-center"> SGST % </th>
                                                                        <th class="text-center"> SGST </th>
                                                                        <th class="text-center"> Total </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                   <tr id="sales-table">
                                                                        <td><input type="text" id="typeahead" name="typeahead" class="form-control form-filter input-sm typeahead" />
                                                                        <input required type="hidden" id="prodid" name="prodid" class="form-control form-filter input-sm prodid" />
                                                                        </td>
                                                                        <td> <input required type="text" readonly class="form-control form-filter input-sm" id="item-code" name="item-code"> </td>
                                                                        <td> <input required type="text" value="0" readonly class="form-control form-filter input-sm" id="item-rateo" name="item-rateo"> </td>
                                                                        <td> <input required type="text" value="0" readonly class="form-control form-filter input-sm" id="item-ratep" name="item-ratep"> </td>
                                                                        <td> <input required type="number" min="0" readonly class="form-control form-filter input-sm" id="item-rate" name="item-rate"> </td>
                                                                       <td> <input required type="text" readonly class="form-control form-filter input-sm" id="item-unit" name="item-unit">
                                                                        </td>
                                                                        <td> <input required type="number" min="0" readonly class="form-control form-filter input-sm" id="item-qty" name="item-qty">
                                                                        <input type="hidden" class="form-control form-filter input-sm" id="item-qtyhi" name="item-qtyhi"> 
                                                                        </td>
                                                                        <td> <input required type="text" value="0" readonly class="form-control form-filter input-sm" id="item-cgstp" name="item-cgstp"> </td>
                                                                        <td> <input required type="text" readonly class="form-control form-filter input-sm" value="0" id="item-cgst" name="item-cgst"> </td>
                                                                         <td> <input required type="text" value="0" readonly class="form-control form-filter input-sm" id="item-sgstp" name="item-sgstp"> </td>
                                                                        <td> <input required type="text" value="0" readonly class="form-control form-filter input-sm" id="item-sgst" name="item-sgst"> </td>
                                                                        <td> <input required type="text" value="0" readonly class="form-control form-filter input-sm" id="item-totall" name="item-totall"> </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        
                                                        <div class="row">                                    
                                                            <div class="alert alert-danger display-hide" id="alert">
                                                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. 
                                                            </div>
                                                            <div class="alert alert-success display-hide" id="alert2">
                                                                <button class="close" data-close="alert"></button> Your Data is successful Saved!
                                                            </div>
                                                        </div>
                                                        <center>
                                                        <div class="btn-group">
                                                            <button type="submit" id="sample_editable_1_new_sales" class="btn green"> Add
                                                            <i class="fa fa-plus"></i>
                                                            </button>
                                                        </div>
                                                        </center>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                        </div>
                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->
                                            <div class="portlet-body">
                                                <table class="table table-striped table-hover table-bordered " id="sample_editable_1_sales">
                                                    <thead>
                                                        <tr class="d-flex" >
                                                            <th> # </th>
                                                            <th class ="text-center col-md-2"> Item </th>
                                                            <th class ="text-center col-md-1"> Code </th>
                                                            <th class="text-center"> Rate </th>
                                                            <th class="text-center"> Unit </th>
                                                            <th class="text-center"> Qty </th>
                                                            <th class="text-center"> CGST % </th>
                                                            <th class="text-center"> CGST </th>
                                                            <th class="text-center"> SGST % </th>
                                                            <th class="text-center"> SGST </th>
                                                            <th class="text-center"> Total </th>
                                                            <th class="text-center"> Delete</th>
                                                            
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php $i = 1; ?>
                                                        @if(count($sales_bill_temps) > 0)
                                                            @foreach($sales_bill_temps as $sales_bill_temp)
                            
                                                                <tr id="{{ $sales_bill_temp->id }}">
                                                                    <td> {{ $i++ }} </td>
                                                                    <?php 
                                                                        $invoice = DB::table('products')
                                                                                    ->select('*')
                                                                                    ->where('id',$sales_bill_temp->product_id)
                                                                                    ->get();
                                                                    ?>
                                                                    <td> {{ $invoice[0]->name}} </td>
                                                                    <td> {{ $invoice[0]->code }} </td>
                                                                    <td> {{ $sales_bill_temp->rate }} </td>
                                                                    <td> {{ $invoice[0]->unit}} </td>
                                                                    <td> {{ $sales_bill_temp->quantity }} </td>
                                                                    <td> {{ ($invoice[0]->gst)/2 }} </td>
                                                                    <td> {{ $sales_bill_temp->cgst }} </td>
                                                                    <td> {{ ($invoice[0]->gst)/2 }} </td>
                                                                    <td> {{ $sales_bill_temp->sgst }} </td>
                                                                    <td> {{ $sales_bill_temp->total }} </td>
                                                                    <td>
                                                                        <a class="deletesales" href="javascript:;"> Delete </a>
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <center>
                                        <div class="form-actions">
                                            <button type="submit" class="btn green">Submit</button>
                                            <a href="{{ route('sales.temp.delete') }}" onclick="javascript:check=confirm( 'Do You Want To Cancel the Bill?'); if(check==false) return false;"><button type="button" class="btn default">Delete</button></a>
                                        </div>
                                        </center>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            @else
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i>
                                    </div>
                                    <!-- <div class="tools">
                                        <a href="javascript:;" class="collapse"> </a>
                                        <a href="javascript:;" class="reload"> </a>
                                    </div> -->
                                </div>
                                <div class="portlet-body">
                                    
                                    <div class="row">                                    
                                        <div class="alert alert-danger display-hide" id="alert">
                                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. 
                                        </div>
                                        <div class="alert alert-success display-hide" id="alert2">
                                            <button class="close" data-close="alert"></button> Your Data is successful Saved!
                                        </div>
                                    </div>
                                    <!-- BEGIN FORM-->
                                    <!-- <form action="#" class="form-horizontal"> -->
                                        <div class="form-body">
                                            <h3 class="form-section">Info</h3>
                                            <div class="row">
                                                
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Date</label>
                                                        <div class="col-md-9">
                                                            <input type="date" class="form-control" id="cudate" name="cudate" required placeholder="Dadte">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                
                                                <!-- Bill number automatic -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Bill</label>
                                                        <div class="col-md-9">
                                                            <input type="text" required class="form-control" id="billno" name="billno" readonly value="{{ $innum }}" placeholder="Automatic">
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <!-- Name -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Name</label>
                                                        <div class="col-md-9">
                                                            <input type="hidden" class="form-control custid" id="custid" name="custid">
                                                            <input type="text" required class="form-control custname" id="custname" name="custname" placeholder="Customer name">
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <!-- phone number  -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Phone</label>
                                                        <div class="col-md-9">
                                                            <input type="number" required class="form-control phone" id="phone" name="phone" placeholder="Phone number ">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->                                            
                                            <br>
                                            <div class="row">                 
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">CGST</label>
                                                        <div class="col-md-9">
                                                            <input type="text" readonly value="0" class="form-control" id="subcgst" name="subcgst">
                                                            <input type="hidden" readonly value="0" class="form-control" id="subcgsttem" name="subcgsttem">
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">SGST</label>
                                                        <div class="col-md-9">
                                                            <input type="text" readonly value="0" class="form-control" id="subsgst" name="subsgst">
                                                            <input type="hidden" readonly value="0" class="form-control" id="subsgsttem" name="subsgsttem">
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">SubTotal</label>
                                                        <div class="col-md-9">
                                                            <input type="text" readonly value="0" class="form-control" id="subtotal" name="subtotal">
                                                            <input type="hidden" readonly value="0" class="form-control" id="subtotaltem" name="subtotaltem">
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                            
                                            <h3 class="form-section">Items</h3>
                                            <!--/row-->
                                            <div class="row" >
                                                <div class="col-md-12">
                                                    <div class="portlet-body">
                                                        <div class="table-responsive" style="height:120px !important;" >
                                                            <table class="table table-striped table-bordered table-hover">
                                                                <thead>
                                                                    <tr class="d-flex" >
                                                                        <th class ="text-center col-md-2"> Item </th>
                                                                        <th class ="text-center  col-md-1"> Code </th>
                                                                        <th class="text-center"> ORate </th>
                                                                        <th class="text-center"> PRate </th>
                                                                        <th class="text-center"> Rate </th>
                                                                        <th class="text-center"> Unit </th>
                                                                        <th class="text-center"> Qty </th>
                                                                        <th class="text-center"> CGST % </th>
                                                                        <th class="text-center"> CGST </th>
                                                                        <th class="text-center"> SGST % </th>
                                                                        <th class="text-center"> SGST </th>
                                                                        <th class="text-center"> Total </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                   <tr id="sales-table">
                                                                        <td><input type="text" id="typeahead" name="typeahead" class="form-control form-filter input-sm typeahead" />
                                                                        <input required type="hidden" id="prodid" name="prodid" class="form-control form-filter input-sm prodid" />
                                                                        </td>
                                                                        <td> <input required type="text" readonly class="form-control form-filter input-sm" id="item-code" name="item-code"> </td>
                                                                        <td> <input required type="text" value="0" readonly class="form-control form-filter input-sm" id="item-rateo" name="item-rateo"> </td>
                                                                        <td> <input required type="text" value="0" readonly class="form-control form-filter input-sm" id="item-ratep" name="item-ratep"> </td>
                                                                        <td> <input required type="number" min="0" readonly class="form-control form-filter input-sm" id="item-rate" name="item-rate"> </td>
                                                                       <td> <input required type="text" readonly class="form-control form-filter input-sm" id="item-unit" name="item-unit">
                                                                       </td>
                                                                        <td> <input required type="number" min="0" readonly class="form-control form-filter input-sm" id="item-qty" name="item-qty">
                                                                        <input type="hidden" class="form-control form-filter input-sm" id="item-qtyhi" name="item-qtyhi"> 
                                                                        </td>
                                                                        <td> <input required type="text" value="0" readonly class="form-control form-filter input-sm" id="item-cgstp" name="item-cgstp"> </td>
                                                                        <td> <input required type="text" readonly class="form-control form-filter input-sm" value="0" id="item-cgst" name="item-cgst"> </td>
                                                                         <td> <input required type="text" value="0" readonly class="form-control form-filter input-sm" id="item-sgstp" name="item-sgstp"> </td>
                                                                        <td> <input required type="text" value="0" readonly class="form-control form-filter input-sm" id="item-sgst" name="item-sgst"> </td>
                                                                        <td> <input required type="text" value="0" readonly class="form-control form-filter input-sm" id="item-totall" name="item-totall"> </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        
                                                        <center>
                                                        <div class="btn-group">
                                                            <button id="sample_editable_1_new_sales" class="btn green"> Add
                                                            <i class="fa fa-plus"></i>
                                                            </button>
                                                        </div>
                                                        </center>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                        </div>
                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->
                                            <div class="portlet-body">
                                                <table class="table table-striped table-hover table-bordered " id="sample_editable_1_sales">
                                                    <thead>
                                                        <tr class="d-flex" >
                                                            <th> # </th>
                                                            <th class ="text-center col-md-2"> Item </th>
                                                            <th class ="text-center col-md-1"> Code </th>
                                                            <th class="text-center"> Rate </th>
                                                            <th class="text-center"> Qty </th>
                                                            <th class="text-center"> CGST % </th>
                                                            <th class="text-center"> CGST </th>
                                                            <th class="text-center"> SGST % </th>
                                                            <th class="text-center"> SGST </th>
                                                            <th class="text-center"> Total </th>
                                                            <!-- <th class="text-center"> Delete</th> -->
                                                            
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- <center>
                                        <div class="form-actions">
                                            <button type="submit" class="btn green">Submit</button>
                                            <button type="button" class="btn default">Cancel</button>
                                        </div>
                                        </center> -->
                                    <!-- </form> -->
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
@endsection