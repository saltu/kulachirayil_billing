$(".custname").autocomplete({
    source: 'customer/custname',
    minLength: 1,
    select: function(event, ui)
    {
      value = ui.item.value;
        // alert(ui.item.value);
        $.ajax({
            type:'get',
            url:'customer/details',
            data:{value:value},
            success:function(data)
            {
                var json = $.parseJSON(data);
                // alert(json['id']);
                $('#custid').val(json['id']);
                $('#phone').val(json['phone']);
            }
        })
    }
});

$(".typeahead").autocomplete({
    source: 'product/prodname',
    minLength: 1,
    select: function(event, ui)
    {
      value = ui.item.value;
        // alert(ui.item.value);
        $.ajax({
            type:'get',
            url:'product/details',
            data:{value:value},
            success:function(data)
            {
                $('#item-qty').attr("readonly", false);
                // $('#item-dis').attr("readonly", false);
                $('#item-rate').attr("readonly", false);
                var json = $.parseJSON(data);
                // alert(json['id']);
                $('#prodid').val(json['id']);
                $('#item-code').val(json['code']);
                var rateo = parseFloat(json['rate']) * parseFloat((json['gst'])/100);
                var tt = parseFloat(rateo + json['rate']);
                $('#item-rateo').val(Math.round(tt));
                $('#item-cgstp').val((json['gst'])/2);
                $('#item-sgstp').val((json['gst'])/2);
                // $('#item-qty').attr('min',0);
                // $('#item-qty').attr('max',json['qty']);
                $('#item-unit').val(json['unit']);
                $('#item-qtyhi').val(json['qty']);
            }
        })
    }
});

$('#phone').on('input',function(e){
    var maxLen = 10;
    var Length = $("#phone").val().length;
    if(Length > maxLen){
            var phone = $("#phone").val();
            $('#phone').css({"border-color":"red"});
            $('#alert').removeClass('display-hide');
            $('#alert').html("Please enter a valid Phone number");
            phone = phone.slice(0,-1);
            $('#phone').val(phone);
    }
});

$('#amountpay').on('input',function(e){
    var payamount = parseFloat ($("#amountpay_temp").val());
    var amount =    parseFloat ($("#amountpay").val());
    if( amount <= payamount)
        {
            console.log('less');
            $('#sales_alert').addClass('display-hide');
            $('#amountpay').css({"border-color":""});
        }
    else
        {
            console.log('higher');
            $('#amountpay').css({"border-color":"red"});
            $('#sales_alert').removeClass('display-hide');
            $('#sales_alert').html("Exceeded pay amount");
            $('#amountpay').val('');
        }
});

$('#item-rate').on('input',function(e){

    var qty1 = $('#item-qty').val();
    var pcgst = $('#item-cgstp').val();
    var psgst = $('#item-sgstp').val();
    var rate = $('#item-rate').val();
    var dis = $('#item-dis').val();
   var tot = $('#item-totall').val();
    
    var subcgst = $('#subcgst').val();
    var subsgst = $('#subsgst').val();
    var subtotal = $('#subtotal').val();

    var subcgsttem = $('#subcgsttem').val();
    var subsgsttem = $('#subsgsttem').val();
    var subtotaltem = $('#subtotaltem').val();
    
    var cgst1 = parseFloat(rate * (pcgst/100));
    var sgst1 = parseFloat(rate * (psgst/100));
    var gst = parseFloat (cgst1) +  parseFloat(sgst1);
    var amo_p = parseFloat(rate - gst);
    $('#item-ratep').val(Math.round(amo_p));
    
    
    if (qty1 == '')
        {
           console.log("null");
           $('#subcgst').val(subcgsttem);
           $('#subsgst').val(subsgsttem);
           if (tot != '0')
               {
                   $('#subtotal').val(subtotaltem); 
               }
//           $('#item-totall').val(subtotaltem);
        }
    else
        {
            var am_tot = parseFloat(rate * qty1); 
            var cgst1_tot = parseFloat(qty1 * cgst1);
            var sgst1_tot = parseFloat(qty1 * sgst1);
            
            var va1 = parseFloat(subcgsttem) +  parseFloat(cgst1_tot);
            var va2 = parseFloat(subsgsttem) + parseFloat(sgst1_tot);
            var va3 = parseFloat(subtotaltem) + parseFloat(am_tot);
            

            $('#item-cgst').val(Math.round(cgst1_tot));
            $('#item-sgst').val(Math.round(sgst1_tot));
            $('#item-totall').val(Math.round(am_tot));
            
            $('#subcgst').val(Math.round(va1));
            $('#subsgst').val(Math.round(va2));
            $('#subtotal').val(Math.round(va3));
         }
});



$('#item-qty').on('input',function(e){
    var qty1 = parseFloat($('#item-qty').val());
    var qty2 = parseFloat($('#item-qtyhi').val());
    var pcgst = $('#item-cgstp').val();
    var psgst = $('#item-sgstp').val();
    var rate = $('#item-rate').val();
    var dis = $('#item-dis').val();
    
    var subcgst = $('#subcgst').val();
    var subsgst = $('#subsgst').val();
    var subtotal = $('#subtotal').val();

    var subcgsttem = $('#subcgsttem').val();
    var subsgsttem = $('#subsgsttem').val();
    var subtotaltem = $('#subtotaltem').val();
    
    var cgst1 = parseFloat(rate * (pcgst/100));
    var sgst1 = parseFloat(rate * (psgst/100));
    var gst = parseFloat (cgst1) +  parseFloat(sgst1);
    
    if (qty1 == '' || qty1 == '0' ){
       console.log("null");
       $('#subcgst').val(subcgsttem);
       $('#subsgst').val(subsgsttem);
       $('#subtotal').val(subtotaltem); 
       $('#item-qty').val(""); 
    } else{
        if(qty1 > qty2){
            
            $('#item-qty').css({"border-color":"red"});
            $('#alert').removeClass('display-hide');
            $('#alert').html("Please enter a valid quatity");
            $('#item-qty').val("");
            e.preventDefault();
            // $('#item-qty').bind("keypress keyup keydown",function(e){
            //     if (e.keyCode === 13 || e.keyCode === 8){
            //         return true;
            //     } else {
            //         return false;
            //     }
            // });
        } else{
            $('#item-qty').css({"border-color":""});
            $('#alert').addClass('display-hide');
            var am_tot = parseFloat(rate * qty1); 
            var cgst1_tot = parseFloat(qty1 * cgst1);
            var sgst1_tot = parseFloat(qty1 * sgst1);
            
            var va1 = parseFloat(subcgsttem) +  parseFloat(cgst1_tot);
            var va2 = parseFloat(subsgsttem) + parseFloat(sgst1_tot);
            var va3 = parseFloat(subtotaltem) + parseFloat(am_tot);
            

            $('#item-cgst').val(Math.round(cgst1_tot));
            $('#item-sgst').val(Math.round(sgst1_tot));
            $('#item-totall').val(Math.round(am_tot));
            
            $('#subcgst').val(Math.round(va1));
            $('#subsgst').val(Math.round(va2));
            $('#subtotal').val(Math.round(va3));
        }        
    }
});

$('#item-dis').on('input',function(e){

    var dis = $('#item-dis').val();
    var qty1 = $('#item-qty').val();
    var cgst = parseFloat($('#item-cgst').val());
    var sgst = parseFloat($('#item-sgst').val());
    var rate = $('#item-rate').val();
    var subtotaltem = $('#subtotaltem').val();
    
    var gst = parseFloat(cgst) + parseFloat(sgst);
    var am = parseFloat(rate * qty1);
    var dis1 = parseFloat(am * (dis/100));
    var amo = parseFloat((am + gst) - dis1);
    
    if(dis  !='' )
        {
            $('#item-totall').val(Math.round(amo));
            var va3 = parseFloat(subtotaltem) + parseFloat(amo);
            $('#subtotal').val(Math.round(va3));
        }
});

// $('#item-qty').input(function(){
//     alert("hello");
//     
// });

var TableDatatablesEditable_sales = function () {

    var handleTable = function () {

        function restoreRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);

            for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                oTable.fnUpdate(aData[i], nRow, i, false);
            }

            oTable.fnDraw();
        }

        function editRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);          
            jqTds[0].innerHTML = '';
            jqTds[1].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[1] + '">';
            jqTds[2].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[2] + '">';
            jqTds[3].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[3] + '">';
            jqTds[4].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[4] + '">';
            jqTds[5].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[5] + '">';
            jqTds[6].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[6] + '">';
            jqTds[7].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[7] + '">';
            jqTds[8].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[8] + '">';
            jqTds[9].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[9] + '">';
            jqTds[10].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[10] + '">';
            jqTds[11].innerHTML = '<a class="cancelsales" href="">Cancel</a>';
        }

        function saveRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 1, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 2, false);
            oTable.fnUpdate(jqInputs[2].value, nRow, 3, false);
            oTable.fnUpdate(jqInputs[3].value, nRow, 4, false);
            oTable.fnUpdate(jqInputs[4].value, nRow, 5, false);
            oTable.fnUpdate(jqInputs[5].value, nRow, 6, false);
            oTable.fnUpdate(jqInputs[6].value, nRow, 7, false);
            oTable.fnUpdate(jqInputs[7].value, nRow, 8, false);
            oTable.fnUpdate(jqInputs[8].value, nRow, 9, false);
            oTable.fnUpdate(jqInputs[9].value, nRow, 10, false);
            oTable.fnUpdate('<a class="deletesales" href="">Delete</a>', nRow, 11, false);
            oTable.fnDraw();
        }

        function cancelEditRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 1, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 2, false);
            oTable.fnUpdate(jqInputs[2].value, nRow, 3, false);
            oTable.fnUpdate(jqInputs[3].value, nRow, 4, false);
            oTable.fnUpdate(jqInputs[4].value, nRow, 5, false);
            oTable.fnUpdate(jqInputs[5].value, nRow, 6, false);
            oTable.fnUpdate(jqInputs[6].value, nRow, 7, false);
            oTable.fnUpdate(jqInputs[7].value, nRow, 8, false);
            oTable.fnUpdate(jqInputs[8].value, nRow, 9, false);
            oTable.fnUpdate('<a class="editsales" href="">Edit</a>', nRow, 10, false);
            oTable.fnDraw();
        }

        var table = $('#sample_editable_1_sales');

        var oTable = table.dataTable({

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

            // set the initial value
            "pageLength": 5,

            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "columnDefs": [{ // set default column settings
                'orderable': true,
                'targets': [0]
            }, {
                "searchable": true,
                "targets": [0]
            }],
            "order": [
                [0, "asc"]
            ] // set first column as a default sort by asc
        });

        var tableWrapper = $("#sample_editable_1_wrapper_sales");

        var nEditing = null;
        var nNew = false;

        $('#sample_editable_1_new_sales').click(function (e) {
            e.preventDefault();

            var item = $('#typeahead').val();
            var item_code = $('#item-code').val();
            var item_ratep = $('#item-ratep').val();
            var item_rate = $('#item-rate').val();
            var item_unit = $('#item-unit').val();
            var item_qty = $('#item-qty').val();
            var item_dis = $('#item-dis').val();
            var item_cgstp = $('#item-cgstp').val();
            var item_sgstp = $('#item-sgstp').val();
            var item_cgst = $('#item-cgst').val();
            var item_sgst = $('#item-sgst').val();
            var item_total = $('#item-totall').val();
            var custname = $('#custname').val();
            var custphone = $('#phone').val();            
            var custid = $('#custid').val();            
            var prodid = $('#prodid').val();          
            var billno = $('#billno').val();         
            var cudate = $('#cudate').val();
            
            var qty1 = parseFloat($('#item-qty').val());
            var qty2 = parseFloat($('#item-qtyhi').val());
            // alert (custname+"jj"+custphone);
            
            if (nNew && nEditing) {
               
                $('#alert2').removeClass("display-hide");
                $('#alert2').html("Previous row not saved. Reloading");
                var delay = 1000; 
                // oTable.fnDeleteRow(nEditing);
                // nEditing = null;
                // nNew = false;
                setTimeout(function(){ window.location = 'salesadd'; }, delay);
                // if (confirm("Previose row not saved. Do you want to save it ?")) {
                //     saveRow(oTable, nEditing); // save
                //     $(nEditing).find("td:first").html("Untitled");
                //     nEditing = null;
                //     nNew = false;

                // } else {
                //     oTable.fnDeleteRow(nEditing); // cancel
                //     nEditing = null;
                //     nNew = false;
                    
                //     return;
                // }
            }

            if(cudate == ''){
                $('#cudate').css({"border-color":"red"});
                $('#cudate').focus();
            } else if(item == ''){
                $('#cudate').css({"border-color":""});
                $('#typeahead').css({"border-color":"red"});
                $('#typeahead').focus();
            }  else if(item_rate == ''){
                $('#typeahead').css({"border-color":""});
                $('#item-rate').focus();
                $('#item-rate').css({"border-color":"red"});
            } else if(item_qty == ''){
                $('#item-rate').css({"border-color":""});
                $('#item-qty').focus();
                $('#item-qty').css({"border-color":"red"});
            }  else if(custname == ''){
                $('#item-qty').css({"border-color":""});
                $('#custname').focus();
                $('#custname').css({"border-color":"red"});
            }  else if(custphone == ''){
                $('#custname').css({"border-color":""});
                $('#phone').focus();
                $('#phone').css({"border-color":"red"});
            } else if(qty1 > qty2){                
                $('#phone').css({"border-color":""});
                $('#item-qty').css({"border-color":"red"});
                $('#alert').removeClass('display-hide');
                $('#alert').html("Please enter a valid quatity");
            } else { 
                $('#item-qty').css({"border-color":""});
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                   type:'POST',
                   url:'temp/add',
                   data: {
                        billno: billno,
                        custid: custid,
                        prodid: prodid,
                        item_rate: item_ratep,
                        item_unit: item_unit,
                        item_qty: item_qty,
                        item_dis: item_dis,
                        item_cgst: item_cgst,
                        item_sgst: item_sgst,
                        item_total: item_total,
                        custname: custname,
                        custphone: custphone,                        
                        cudate: cudate
                    },
                   success:function(data) {
                      window.location = 'salesadd';
                   }
                });
                var aiNew = oTable.fnAddData(['', item, item_code, item_ratep, item_unit, item_qty, item_cgstp, item_cgst, item_sgstp, item_sgst, item_total ,'<a class="delete" href="">Delete</a>' ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                // savedataRow(oTable, nRow);
                // $('#sales-table')[0].reset();
                nEditing = nRow;
                nNew = true;
            }            
        });

        table.on('click', '.deletesales', function (e) {
            e.preventDefault();

            if (confirm("Are you sure to delete this Item ?") == false) {
                return;
            }

            var nRow = $(this).parents('tr').attr('id');
            // alert(nRow);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
               type:'POST',
               url:'temp/delete/data',
               data: { id: nRow },
               success:function(data) {
                  $('#alert2').html(data.success);
                  window.location = 'salesadd';
               }
            });
            oTable.fnDeleteRow(nRow);
            // alert("Deleted! Do not forget to do some ajax to sync with backend :)");

        });

        table.on('click', '.cancelsales', function (e) {
            e.preventDefault();
            if (nNew) {
                oTable.fnDeleteRow(nEditing);
                nEditing = null;
                nNew = false;
            } else {
                restoreRow(oTable, nEditing);
                nEditing = null;
            }
        });

        // table.on('click', '.editsales', function (e) {
        //     e.preventDefault();
        //     nNew = false;
            
        //     /* Get the row as a parent of the link that was clicked on */
        //     var nRow = $(this).parents('tr')[0];

        //     if (nEditing !== null && nEditing != nRow) {
        //         /* Currently editing - but not this row - restore the old before continuing to edit mode */
        //         // $('#sample_editable_1_new_sales').addClass('display-hide')
        //         restoreRow(oTable, nEditing);
        //         editRow(oTable, nRow);
        //         nEditing = nRow;
        //         alert("backend :)");
        //     } else if (nEditing == nRow) {
        //         /* Editing this row and want to save it */
        //         saveRow(oTable, nEditing);
        //         nEditing = null;
        //         alert("Updated! Do not forget to do some ajax to sync with backend :)");
        //     } else {
        //         /* No edit in progress - let's start one */

        //         // $('#sample_editable_1_new_sales').addClass('display-hide')
        //         editRow(oTable, nRow);
        //         nEditing = nRow;
        //         alert("Updated:)");
        //     }
        // });
    }

    return {

        //main function to initiate the module
        init: function () {
            handleTable();
        }

    };

}();

jQuery(document).ready(function() {
    TableDatatablesEditable_sales.init();
});