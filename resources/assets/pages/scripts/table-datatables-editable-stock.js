// $(".purchased").autocomplete({
//     source: 'purchaser/purname',
//     minLength: 1,
//     select: function(event, ui)
//     {
//       value = ui.item.value;
//         // alert(ui.item.value);
//     }
// });



//Stock


function code(){
    var code = $('#code').val();
    if(code == '')
        {
            $('#name').val('');
            $('#gst').val('');
        }
    else{
//    console.log(code);
            $.ajax({
                type:'get',
                url:'stock/product/stockprodcode',
                data:{value:code},
                success:function(data)
                {
                    $('#name').val('');
                    $('#json-datalist').empty();
                    $('#json-namedatalist').empty();
                    var json = $.parseJSON(data);
//                    console.log(data);
                    for(var i = 0; i < json.length; i++){
                        $('#json-datalist').append($("<option></option>")
                                          .attr("value",json[i].hsncode)
                                          .text(json[i].hsncode));
                        
                        $('#json-namedatalist').append($("<option></option>")
                                          .attr("value",json[i].name)
                                          .text(json[i].name));
                        
                        $('#gst').val(json[i].gstrate);
                        $('#gst').attr('readonly',true);
                    }
                }
            })
        }
}

var TableDatatablesEditable_st = function () {

    var handleTable = function () {

        function restoreRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);

            for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                oTable.fnUpdate(aData[i], nRow, i, false);
            }

            oTable.fnDraw();
        }

        function editRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);            
            jqTds[0].innerHTML = '';
            jqTds[1].innerHTML = '<input type="text" oninput="code()" id="code" placeholder="Product Code" required class="form-control input-small" list="json-datalist" value="' + aData[1] + '"><datalist id="json-datalist"> </datalist>' ;
            jqTds[2].innerHTML = '<input type="text" id="name" placeholder="Product Name" required class="form-control input-small" list="json-namedatalist" value="' + aData[2] + '"><datalist id="json-namedatalist"> </datalist>';
            jqTds[3].innerHTML = '<input type="text" readonly id="purchased" placeholder="Purchased From" required class="form-control input-small purchased" value="' + aData[3] + '">';
            jqTds[4].innerHTML = '<input type="number" id="gst" placeholder="Product GST" pattern="[0-9]" required class="form-control input-small" value="' + aData[4] + '">';
            
            jqTds[5].innerHTML = '<select name ="unit" id="unit" class="form-control form-filter input-sm col-md-1" ><option value="'+ aData[5] +'">'+ aData[5] +' </option> <option value="gram">Gram</option><option value="kg">KG</option><option value="litre">Litre</option><option value="pieces">Pieces</option><option value="box">Box</option><option value="Sqft">Sqft</option></select>';
            
            jqTds[6].innerHTML = '<input type="number" id="quantity" placeholder="Product Quantity" pattern="[0-9]" required class="form-control input-small" value="' + aData[6] + '">';
            jqTds[7].innerHTML = '<input type="number" id="rate" placeholder="Product Rate" pattern="[0-9]" required class="form-control input-small" value="' + aData[7] + '">';
            jqTds[8].innerHTML = '<a class="edit" href=""><i class="fa fa-floppy-o" aria-hidden="true"></i></a>';
            jqTds[9].innerHTML = '<a class="cancel" href=""><i class="fa fa-times" aria-hidden="true"></i></a>';
        }

        function saveRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            var jqInputss = $('select', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 1, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 2, false);
            oTable.fnUpdate(jqInputs[2].value, nRow, 3, false);
            oTable.fnUpdate(jqInputs[3].value, nRow, 4, false);
            oTable.fnUpdate(jqInputss[0].value, nRow, 5, false);
            oTable.fnUpdate(jqInputs[4].value, nRow, 6, false);
            oTable.fnUpdate(jqInputs[5].value, nRow, 7, false);
            oTable.fnUpdate('<a class="edit" href=""><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>', nRow, 8, false);
            oTable.fnUpdate('<a class="delete" href=""><i class="fa fa-trash" aria-hidden="true"></i></a>', nRow, 9, false);
            oTable.fnDraw();
        }

        function cancelEditRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            var jqInputss = $('select', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 1, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 2, false);
            oTable.fnUpdate(jqInputs[2].value, nRow, 3, false);
            oTable.fnUpdate(jqInputs[3].value, nRow, 4, false);
            oTable.fnUpdate(jqInputss[0].value, nRow, 5, false);
            oTable.fnUpdate(jqInputs[4].value, nRow, 6, false);
            oTable.fnUpdate(jqInputs[5].value, nRow, 7, false);
            oTable.fnUpdate(jqInputs[6].value, nRow, 7, false);
            oTable.fnUpdate('<a class="edit" href=""><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>', nRow, 8, false);
            oTable.fnDraw();
        }

        var table = $('#sample_editable_1_st');

        var oTable = table.dataTable({

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            // "sScrollX": "90%",
            // "sScrollXInner": "100%",
            // "bScrollCollapse": true,
            // "dom": 'Bfrtip',
            "buttons": [
               {
                    "text": '<i class="fa fa-lg fa-file-excel-o"></i>',
                    "extend": 'excel',
                    "className": 'btn btn-xs btn-primary p-5 m-0 width-35 assets-export-btn export-xls ttip',
                    "title": 'EyesOn Product Details',
                    "extension": '.xls'
                }, {
                    "text": '<i class="fa fa-lg fa-file-pdf-o"></i>',
                    "extend": 'pdf',
                    "className": 'btn btn-xs btn-primary p-5 m-0 width-35 assets-export-btn export-pdf ttip',
                    "title": 'EyesOn Product Details',
                    "extension": '.pdf'
                }, {
                    "text": '<i class="fa fa-lg fa-print"></i>',
                    "extend": 'print',
                    "className": 'btn btn-xs btn-primary p-5 m-0 width-35 assets-export-btn export-pdf ttip',
                     "title": 'EyesOn Product Details'
                }
            ],

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

            // set the initial value
            "pageLength": 5,

            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "columnDefs": [{ // set default column settings
                'orderable': true,
                'targets': [0]
            }, {
                "searchable": true,
                "targets": [0]
            }],
            "order": [
                [0, "asc"]
            ] // set first column as a default sort by asc
        });

        var tableWrapper = $("#sample_editable_1_wrapper_st");

        var nEditing = null;
        var nNew = false;

        $('#sample_editable_1_new_st').click(function (e) {
            e.preventDefault();

            if (nNew && nEditing) {

                $('#alert2').removeClass("display-hide");
                $('#alert2').html("Previous row not saved. Reloading");
                var delay = 1000; 
                // oTable.fnDeleteRow(nEditing);
                // nEditing = null;
                // nNew = false;
                setTimeout(function(){ window.location = 'stock'; }, delay);


                // if (confirm("Previose row not saved. Do you want to save it ?")) {
                //     // saveRow(oTable, nEditing); // save
                //     window.location = 'stock';
                //     // $(nEditing).find("td:first").html("Untitled");
                //     nEditing = null;
                //     nNew = false;

                // } else {
                //     oTable.fnDeleteRow(nEditing); // cancel
                //     nEditing = null;
                //     nNew = false;
                    
                //     return;
                // }
            }

            var aiNew = oTable.fnAddData(['','', '', '', '', '', '', '', '','']);
            var nRow = oTable.fnGetNodes(aiNew[0]);
            editRow(oTable, nRow);
            nEditing = nRow;
            nNew = true;
        });

        table.on('click', '.delete', function (e) {
            e.preventDefault();

            if (confirm("Are you sure to delete this row ?") == false) {
                return;
            }

            var nRow = $(this).parents('tr').attr('id');
            // alert(nRow);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
               type:'POST',
               url:'stock/delete',
               data: { id: nRow },
               success:function(data) {
                  $('#alert2').removeClass("display-hide");
                  $('#alert2').html(data.success);
                  window.location = 'stock';
               }
            });

            oTable.fnDeleteRow(nRow);
            // alert("Deleted! Do not forget to do some ajax to sync with backend :)");
        });

        table.on('click', '.cancel', function (e) {
            e.preventDefault();
            if (nNew) {
                oTable.fnDeleteRow(nEditing);
                nEditing = null;
                nNew = false;
                window.location = 'stock';
            } else {
                restoreRow(oTable, nEditing);
                nEditing = null;
                window.location = 'stock';
            }
        });

        table.on('click', '.edit', function (e) {
            e.preventDefault();
            nNew = false;
            
            /* Get the row as a parent of the link that was clicked on */
            var nRow = $(this).parents('tr')[0];
            var nRow_id = $(this).parents('tr').attr('id');

            if (nEditing !== null && nEditing != nRow) {

                $('#sample_editable_1_new_st').addClass('display-hide')
                /* Currently editing - but not this row - restore the old before continuing to edit mode */
                restoreRow(oTable, nEditing);
                editRow(oTable, nRow);
                nEditing = nRow;
            } else if (nEditing == nRow) {
                /* Editing this row and want to save it */
                var e = document.getElementById("unit");
                var strUser = e.options[e.selectedIndex].text;
                var data = $('input', nEditing);
                // console.log(strUser);
                // console.log(data);
                var code = data[0].value;
                var name = data[1].value;
                var purchased = data[2].value;
                var gst = data[3].value;
                var unit = strUser;
                var quantity = data[4].value;
                var rate = data[5].value;
                // alert(code+name+purchased+gst+quantity+rate);
                // else if(purchased == ''){
                //     $('#name').css({"border-color":""});
                //     $('#purchased').focus();
                //     $('#purchased').css({"border-color":"red"});
                //     $('#alert').removeClass("display-hide");
                //     $('#alert').html("Please enter the Product Purchased From");
                // }

                if(code == ''){
                    $('#code').css({"border-color":"red"});
                    $('#code').focus();
                    $('#alert').removeClass("display-hide");
                    $('#alert').html("Please enter the Product Code");
                } else if(name == ''){
                    $('#code').css({"border-color":""});
                    $('#name').focus();
                    $('#name').css({"border-color":"red"});
                    $('#alert').removeClass("display-hide");
                    $('#alert').html("Please enter the Product Name");
                }  else if(gst == ''){
                    $('#name').css({"border-color":""});
                    $('#gst').focus();
                    $('#gst').css({"border-color":"red"});
                    $('#alert').removeClass("display-hide");
                    $('#alert').html("Please enter the Product GST");
                } else if(quantity == ''){
                    $('#gst').css({"border-color":""});
                    $('#quantity').focus();
                    $('#quantity').css({"border-color":"red"});
                    $('#alert').removeClass("display-hide");
                    $('#alert').html("Please enter the Product Quantity");
                } else if(rate == ''){
                    $('#quantity').css({"border-color":""});
                    $('#rate').focus();
                    $('#rate').css({"border-color":"red"});
                    $('#alert').removeClass("display-hide");
                    $('#alert').html("Please enter the Product Rate");
                } else if(unit == ''){
                    $('#rate').css({"border-color":""});
                    $('#unit').focus();
                    $('#unit').css({"border-color":"red"});
                    $('#alert').removeClass("display-hide");
                    $('#alert').html("Select a Unit");
                } else { 
                    $('#unit').css({"border-color":""});
                    $('#alert').addClass("display-hide");
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                       type:'POST',
                       url:'stock/add',
                       data: {
                            code: code,
                            name: name,
                            gst: gst,
                            quantity: quantity,
                            rate: rate,
                            id: nRow_id,
                            unit: unit,
                        },
                       success:function(data) {
                          $('#alert2').removeClass("display-hide");
                          $('#alert2').html(data.success);
                          window.location = 'stock';
                       }
                    });
                    saveRow(oTable, nEditing);
                    nEditing = null;
                }
                // alert("Updated! Do not forget to do some ajax to sync with backend :)");
            } else {
                /* No edit in progress - let's start one */
                $('#sample_editable_1_new_st').addClass('display-hide')
                editRow(oTable, nRow);
                nEditing = nRow;
            }
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleTable();
        }

    };

}();

jQuery(document).ready(function() {
    TableDatatablesEditable_st.init();
});