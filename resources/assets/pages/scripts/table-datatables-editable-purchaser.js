
//Stock
var TableDatatablesEditablepur = function () {

    var handleTable = function () {

        function restoreRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);

            for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                oTable.fnUpdate(aData[i], nRow, i, false);
            }

            oTable.fnDraw();
        }

        function editRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);            
            jqTds[0].innerHTML = '';
            jqTds[1].innerHTML = '<input type="text" id="name" placeholder="Name" required class="form-control input-small" value="' + aData[1] + '">';
            jqTds[2].innerHTML = '<input type="text" id="phone" placeholder="Phone" required class="form-control input-small" value="' + aData[2] + '">';
            jqTds[3].innerHTML = '<input type="text" id="gst" placeholder="GST" required class="form-control input-small" value="' + aData[3] + '">';
            jqTds[4].innerHTML = '<a class="editpur" href=""><i class="fa fa-floppy-o" aria-hidden="true"></i></a>';
            jqTds[5].innerHTML = '<a class="cancelpur" href=""><i class="fa fa-times" aria-hidden="true"></i></a>';
        }

        function saveRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 1, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 2, false);
            oTable.fnUpdate(jqInputs[2].value, nRow, 3, false);
            oTable.fnUpdate('<a class="editpur" href=""><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>', nRow, 4, false);
            oTable.fnUpdate('<a class="deletepur" href=""><i class="fa fa-trash" aria-hidden="true"></i></a>', nRow, 5, false);
            oTable.fnDraw();
        }

        function cancelEditRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 1, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 2, false);
            oTable.fnUpdate(jqInputs[2].value, nRow, 3, false);
            oTable.fnUpdate('<a class="editpur" href=""><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>', nRow, 4, false);
            oTable.fnDraw();
        }

        var table = $('#sample_editable_1_pur');

        var oTable = table.dataTable({

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            // "sScrollX": "90%",
            // "sScrollXInner": "100%",
            // "bScrollCollapse": true,
            // "dom": 'Bfrtip',
            "buttons": [
               {
                    "text": '<i class="fa fa-lg fa-file-excel-o"></i>',
                    "extend": 'excel',
                    "className": 'btn btn-xs btn-primary p-5 m-0 width-35 assets-export-btn export-xls ttip',
                    "title": 'EyesOn Product Details',
                    "extension": '.xls'
                }, {
                    "text": '<i class="fa fa-lg fa-file-pdf-o"></i>',
                    "extend": 'pdf',
                    "className": 'btn btn-xs btn-primary p-5 m-0 width-35 assets-export-btn export-pdf ttip',
                    "title": 'EyesOn Product Details',
                    "extension": '.pdf'
                }, {
                    "text": '<i class="fa fa-lg fa-print"></i>',
                    "extend": 'print',
                    "className": 'btn btn-xs btn-primary p-5 m-0 width-35 assets-export-btn export-pdf ttip',
                     "title": 'EyesOn Product Details'
                }
            ],

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

            // set the initial value
            "pageLength": 5,

            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "columnDefs": [{ // set default column settings
                'orderable': true,
                'targets': [0]
            }, {
                "searchable": true,
                "targets": [0]
            }],
            "order": [
                [0, "asc"]
            ] // set first column as a default sort by asc
        });

        var tableWrapper = $("#sample_editable_1_wrapper");

        var nEditing = null;
        var nNew = false;

        $('#sample_editable_1_new_pur').click(function (e) {
            e.preventDefault();

            if (nNew && nEditing) {

                $('#alert2').removeClass("display-hide");
                $('#alert2').html("Previous row not saved. Reloading");
                var delay = 1000; 
                setTimeout(function(){ window.location = 'purchaser'; }, delay);

                // if (confirm("Previose row not saved. Do you want to save it ?")) {
                //     // saveRow(oTable, nEditing); // save
                //     window.location = 'stock';
                //     // $(nEditing).find("td:first").html("Untitled");
                //     nEditing = null;
                //     nNew = false;

                // } else {
                //     oTable.fnDeleteRow(nEditing); // cancel
                //     nEditing = null;
                //     nNew = false;
                    
                //     return;
                // }
            }

            var aiNew = oTable.fnAddData(['','', '', '', '','']);
            var nRow = oTable.fnGetNodes(aiNew[0]);
            editRow(oTable, nRow);
            nEditing = nRow;
            nNew = true;
        });

        table.on('click', '.deletepur', function (e) {
            e.preventDefault();

            if (confirm("Are you sure to delete this row ?") == false) {
                return;
            }

            var nRow = $(this).parents('tr').attr('id');
            // alert(nRow);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
               type:'POST',
               url:'purchaser/delete',
               data: { id: nRow },
               success:function(data) {
                  $('#alert2').removeClass("display-hide");
                  $('#alert2').html(data.success);
                  window.location = 'purchaser';
               }
            });

            oTable.fnDeleteRow(nRow);
            // alert("Deleted! Do not forget to do some ajax to sync with backend :)");
        });

        table.on('click', '.cancelpur', function (e) {
            e.preventDefault();
            if (nNew) {
                oTable.fnDeleteRow(nEditing);
                nEditing = null;
                nNew = false;
            } else {
                restoreRow(oTable, nEditing);
                nEditing = null;
            }
        });

        table.on('click', '.editpur', function (e) {
            e.preventDefault();
            nNew = false;
            $('#sample_editable_1_new_pur').addClass('display-hide')
            /* Get the row as a parent of the link that was clicked on */
            var nRow = $(this).parents('tr')[0];
            var nRow_id = $(this).parents('tr').attr('id');

            if (nEditing !== null && nEditing != nRow) {
                /* Currently editing - but not this row - restore the old before continuing to edit mode */
                restoreRow(oTable, nEditing);
                editRow(oTable, nRow);
                nEditing = nRow;
            } else if (nEditing == nRow ) {
                /* Editing this row and want to save it */
                var data = $('input', nEditing);
                var name = data[0].value;
                var phone = data[1].value;
                var gst = data[2].value;
                // alert(code+name+purchased+gst+quantity+rate);
                if(name == ''){
                    $('#name').css({"border-color":"red"});
                    $('#name').focus();
                    $('#alert').removeClass("display-hide");
                    $('#alert').html("Please enter the Name");
                } else if(phone == ''){
                    $('#name').css({"border-color":""});
                    $('#phone').focus();
                    $('#phone').css({"border-color":"red"});
                    $('#alert').removeClass("display-hide");
                    $('#alert').html("Please enter the Phone");
                } else if(gst == ''){
                    $('#phone').css({"border-color":""});
                    $('#gst').focus();
                    $('#gst').css({"border-color":"red"});
                    $('#alert').removeClass("display-hide");
                    $('#alert').html("Please enter the GST TIN");
                } else {
                    $('#gst').css({"border-color":""});
                    $('#alert').addClass("display-hide");
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                       type:'POST',
                       url:'purchaser/add',
                       data: {
                            name: name,
                            phone: phone,
                            id: nRow_id,
                            gst: gst
                        },
                       success:function(data) {
                          $('#alert2').removeClass("display-hide");
                          $('#alert2').html(data.success);
                          window.location = 'purchaser';
                       }
                    });
                    saveRow(oTable, nEditing);
                    nEditing = null;
                }
                // alert("Updated! Do not forget to do some ajax to sync with backend :)");
            } else {
                /* No edit in progress - let's start one */
                editRow(oTable, nRow);
                nEditing = nRow;
            }
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleTable();
        }

    };

}();

jQuery(document).ready(function() {
    TableDatatablesEditablepur.init();
});