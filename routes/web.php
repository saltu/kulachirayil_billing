<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {})->name('/');

// Main Page
Route::get('/', 'Controller@index')->name('/');

Auth::routes();

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});

//login
Route::get('/login', 'Auth\LoginController@index')->name('login');
Route::post ('/login', 'Auth\LoginController@login')->name('login');
Route::get ( '/logout', 'Auth\LoginController@logout' )->name('logout');
Route::get('/register', function () {
    return redirect('/');
});

//Home Page
Route::get('/home', 'HomeController@index')->name('home');

//Backup
Route::get('backup','HomeController@backup')->name('backup');

//Customer
Route::prefix('customer')->group(function(){
    Route::get('/', 'CustomerController@index')->name('customer');
    Route::post('/add', 'CustomerController@add')->name('customer.add');
    Route::post('/delete', 'CustomerController@delete')->name('customer.delete');
});

//Purchaser
Route::prefix('purchaser')->group(function(){
    Route::get('/', 'PurchaserController@index')->name('purchaser');
    Route::post('/add', 'PurchaserController@add')->name('purchaser.add');
    Route::post('/delete', 'PurchaserController@delete')->name('purchaser.delete');
});

//Stocks
Route::prefix('stock')->group(function(){
    Route::get('/', 'StocksController@index')->name('stock');    
    Route::post('/add', 'StocksController@add')->name('stock.add');
    Route::post('/delete', 'StocksController@delete')->name('stock.delete');
    //autocomplete product
    Route::get('product/stockprodcode', 'StocksController@stockprodcode')->name('stock.product.stockprodcode');
    
});

//Expense
Route::prefix('expense')->group(function(){
    Route::get('/', 'ExpenseController@index')->name('expense');
    Route::post('/add', 'ExpenseController@add')->name('expense.add');
    Route::post('/delete', 'ExpenseController@delete')->name('expense.delete');
});

//Sales
Route::prefix('sales')->group(function(){
    Route::get('salesadd', 'SalesController@salesadd')->name('sales.salesadd');
    // Bill Save
    Route::post('add', 'SalesController@salesaddmain')->name('sales.add');
    //Temp
    Route::post('temp/add', 'SalesController@tempadd')->name('sales.temp.add');
    Route::post('temp/details', 'SalesController@tempdetails')->name('sales.temp.details');
    Route::get('temp/delete', 'SalesController@tempdelete')->name('sales.temp.delete');
    Route::post('temp/delete/data', 'SalesController@tempdeletedata')->name('sales.temp.delete.data');
    //Report
    Route::get('salesreport', 'SalesController@salesreport')->name('sales.salesreport');
    Route::post('salesreport/data', 'SalesController@salesreportdata')->name('sales.salesreport.data');
    Route::get('salesreport/details/{billno}', 'SalesController@salesreportdetails')->name('sales.salesreport.details');
    //autocomplete customer
    Route::get('customer/custname', 'CustomerController@custname')->name('sales.customer.custname');
    Route::get('customer/details', 'CustomerController@custdetails')->name('sales.customer.details');
    //autocomplete product
    Route::get('product/prodname', 'StocksController@prodname')->name('sales.product.prodname');
    Route::get('product/details', 'StocksController@proddetails')->name('sales.product.details');
    // Return
    Route::get('salesentry', 'SalesController@salesentry')->name('sales.salesentry');

});

//Purchase
Route::prefix('purchase')->group(function(){
    Route::get('purchaseadd', 'PurchaseController@purchaseadd')->name('purchase.purchaseadd');    
    // Bill Save
    Route::post('add', 'PurchaseController@purchaseaddmain')->name('purchase.add');    
    //Temp
    Route::post('temp/add', 'PurchaseController@tempadd')->name('purchase.temp.add');
    Route::post('temp/details', 'PurchaseController@tempdetails')->name('purchase.temp.details');
    Route::get('temp/delete', 'PurchaseController@tempdelete')->name('purchase.temp.delete');
    Route::post('temp/delete/data', 'PurchaseController@tempdeletedata')->name('purchase.temp.delete.data');
    //Report
    Route::get('purchasereport', 'PurchaseController@purchasereport')->name('purchase.purchasereport');
    Route::post('purchasereport/data', 'PurchaseController@purchasereportdata')->name('purchase.purchasereport.data');
    Route::get('purchasereport/details/{billno}', 'PurchaseController@purchasereportdetails')->name('purchase.purchasereport.details');
    //autocomplete purchaser
    Route::get('purchaser/purname', 'PurchaserController@purname')->name('purchase.purchaser.purname');
    Route::get('purchaser/details', 'PurchaserController@purdetails')->name('purchase.purchaser.details');
    //autocomplete product
    Route::get('product/prodname', 'StocksController@prodname')->name('purchase.product.prodname');
        //by name
        Route::get('product/prodcodename', 'PurchaseController@prodcodename')->name('purchase.product.prodcodename');
        Route::get('product/codenamedetails', 'PurchaseController@codenamedetails')->name('purchase.product.codenamedetails');
        //by code
        Route::get('product/prodcode', 'PurchaseController@prodcode')->name('purchase.product.prodcode');
        Route::get('product/codedetails', 'PurchaseController@codedetails')->name('purchase.product.codedetails');
    Route::get('product/details', 'StocksController@proddetails')->name('purchase.product.details');

});

//Payment
Route::prefix('payment')->group(function(){
    //Sales
    Route::get('sales', 'SalesPaymentController@index')->name('payment.sales');
    Route::get('psbill/no', 'SalesPaymentController@billno')->name('payment.sales.billno');
    Route::get('psbill/details', 'SalesPaymentController@billdetails')->name('payment.sales.bill.details');
    Route::post('psbill/add', 'SalesPaymentController@billadd')->name('payment.sales.bill.add');
    
    //Purchase
    Route::get('purchase', 'PurchasePaymentController@index')->name('payment.purchase');
    Route::get('ppbill/no', 'PurchasePaymentController@billno')->name('payment.purchase.billno');
    Route::get('ppbill/details', 'PurchasePaymentController@billdetails')->name('payment.purchase.bill.details');
    Route::post('ppbill/add', 'PurchasePaymentController@billadd')->name('payment.purchase.bill.add');

    //Customer
    Route::get('customer', 'SalesPaymentController@customer')->name('payment.customer');
    //autocomplete customer
    Route::get('customer/custname', 'CustomerController@custname')->name('sales.customer.custname');
    Route::get('customer/details', 'CustomerController@custdetails')->name('sales.customer.details');
    //get bill no
    Route::get('cust/billno', 'SalesPaymentController@custbillno')->name('payment.customer.billno');

    //Purchaser
    Route::get('purchaser', 'PurchasePaymentController@purchaser')->name('payment.purchaser');
    //autocomplete purchaser
    Route::get('purchaser/purname', 'PurchaserController@purname')->name('purchase.purchaser.purname');
    Route::get('purchaser/details', 'PurchaserController@purdetails')->name('purchase.purchaser.details');
    //get bill no
    Route::get('purc/billno', 'PurchasePaymentController@purcbillno')->name('payment.purchaser.billno');

});

//DailyReport
Route::prefix('dailyreport')->group(function(){
    //Daily
    Route::get('report', 'DailyReportController@index')->name('dailyreport.daily');
    Route::post('report/details', 'DailyReportController@reportdetails')->name('dailyreport.daily.details');
});